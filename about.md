---
title: About 
---

Hi, I'm Ev Bogue.

I'm interested in [building perfect machines](http://build.evbogue.com) using open source technologies. I want people to have the expertise to design computers they can have control over, instead of using cloud-based computers that have control over them. 

This is a technical problem, and it means learning more about how computers work in order to build, install, and configure better software on our machines.

I'm from Nueva York. Right now, I live in Mexico City. But all of my stuff fits in one bag, so I tend to move around often.

I've been called a douchebag in the past, this is because I can sometimes be very blunt with people. I'm not very good at being political, this is why I'm a solitary writer who travels the world.

I believe problems can be solved with better code, and hope to write in a way that helps people learn how to solve their problems by deploying better code.

These days, it's hard to be creative without [designing your own website](http://design.evbogue.com). When you deploy your own website to the distributed web, you can say what is on your mind, and connect with people across the world. This is pretty cool, and so I hope to help you learn better design fundamentals.

-Ev
