---
title: 7 questions about my current stack 
date: 2014-02-19
---

Things change on my [perfect machine](http://build.evbogue.com), otherwise it might fall into the less-than-perfect catagory. 

Below I answer 7 questions about my perfect machine. These questions were asked by [Gwen Bell](http://gwenbell.com), the author of [Git Commit](http://git.gwenbell.com) and [Align Your Website](http://align.gwenbell.com).

**How has your stack changed since the last time I checked in with you?**

My stack hasn't changed too much since the last time we talked. I use [Arch Linux](http://archlinux.org/). I've added [Bitmessage](http://bitmessage.org/) for communication. Bitmessage seems more ready for use than [Twister](http://twister.net.co), but it's also been around longer. I'm still using [Xmonad](http://xmonad.org) as my window manager, and [Terminology](/terminology) as my terminal emulator. For my web browser I've been alternating between [dwb](http://portix.bitbucket.org/dwb/) and [Tor](https://www.torproject.org/). I've known about and used Tor for two+ years now, but I've only started trying to use it as my primary browser in recent weeks.

I continue to experiment with my perfect stack. For example, I tried the [Awesome](http://awesome.naquadah.org/) window manager again a few days ago, and it still didn't appeal to me. 

**What's your favorite app right now? Why?**

Bitmessage. I started using it on the same day I announced my embargo of Gmail. I've been surprised how simple it is for most people to install. Even Windows and Mac OS not-so-seXy-anymore.

**What is this dwb thing I see you using? What are its strengths and weaknesses?**

It's a Webkit browser for visiting web pages. I've been trying it out, since Firefox is about to [jump the shark with sponsored ads](http://mashable.com/people/jasona/).  I guess Mozilla is worried Google is going to crash and their benefactor won't be able to afford to keep them around anymore. 

The main benefit of dwb is the Vim/Emacs-ish keybindings, and the minimal interface. No giant menus to take up my screen. Also it asks me if I want to enable flash on every website I visit that is using it, so I get to opt myself in to playing videos and flash-based spyware. 

**What's the first thing you do on your perfect machine in the morning?**

Ideally I'd write a little first in Vim. But most of the time I check to see what messages I've gotten in Mutt using

	$ getmail

And then I fire up Bitmessage to see if anyone has Bitmessaged me. 

I tend to answer emails and messages first, and then get to work.

**If you could change one thing about your stack setup, what would it be? Software/hardware, what?**

I wish I had a decent p2p microblogging app I could use to reach readers. There just isn't a decent one, and I'm not an NSA-compliant cocksucker, so I won't use centralized social networks. Bitmessage does have a broadcast/subscribe function, so I've been using that to send out new posts. You can subscribe to me at this address to receive broadcasts on Bitmessage: BM-2cV8cTUxCx5uFFyNy2E3mo66Xp3nrZFFG2 

Reply to my broadcasts and let me know what you think of the post.

**Tell me about Bitmessage. What have you learned? What are its strengths, what are its weaknesses?**

Bitmessage is an (in theory) uncensorable way to communicate in a secure way from person to person. It's a direct replacement for email, with some other features such as anonymous Chans (messageboards/channels) and a broadcast function. You can run Bitmessage over Tor, and I do, so it's difficult to discover where the Bitmessages have been sent from. You can switch identities as much as you want. It's challenging for someone spying on the network to know anything about the messages being sent and received, because they get passed around to all of the Nodes, and there is no metadata available (which is what the gov thinks they're entitled to.) and only as long as you keep your private key safe no one can decrypt the messages sent to you until quantum computers are invented or some other way to break the encryption is discovered.

One interesting feature that goes above and beyond email is you can see when a message has been received by the person you sent it to. This is cool, because you know when someone has turned off Bitmessage for a week and doesn't get your message.

Messages also expire after a set time, so the network doesn't get bogged down with tons of messages.

Weaknesses: people are concerned Bitmessage won't scale to a large number of users. 

**What are you working on right now? How is that going?**

Over the next two weeks I'm finishing up [Build Your Perfect Machine](http://build.evbogue.com). I've been working on it since October, and I've received a decent amount of feedback from clients who have used it to build their perfect machines. So I figure it's time to wrap up the project. It'll be done, and stay available, at a slightly more expensive price once I'm done with it. -Ev



