---
title: Everybody knows Google is considered harmful
published: 2013-11-20
---

It's been more than a year since I deleted all of my accounts on Google products. With the exception of using Youtube briefly in New York to run a one-month podcast called Google is Stupid -- now defunct, because that pseudononymous account had to be deleted as well.

It's always hard to correct our perceptions after discovering a once beautiful service has turned into a useless evil megacorporation without regard for Internet users' quality of life. But we must. When Google asked me for a cell phone number to get back into my account after a short time away, I knew it was time to delete the account.

Knowing your phone number is essential for tracking your physical location at all times without your permission.

And yet I keep getting emails from people with Gmail addresses. No doubt these people don't have the technical skills to leave. But we must find better ways to communicate than one centralized system that is either handing our information direct to the United States' government, or at least being illegally accessed by the United States government. We can do better than this.

The good news is, there are plenty of alternatives to Google products at this point. Now that everybody knows Google is considered harmful, it's time to make the switch to these products.

Not every situation will be a one-for-one Google replacement. You might find some solutions are better than others. Using Git is an amazing, and in many ways superior alternative to using Google Docs. As long as you don't fall into the trap of using Github. 

WebRTC is an up-and-coming alternative to the locked-in approach of Google Hangouts. It's, of course, decentralized, peer to peer, and doesn't pipe your face direct to the NSA to be preserved from now until woolly mammoths begin roaming the Earth again.

No one uses Google Talk anymore, because everybody knows it is no longer compatible with any services outside of Google. If you use Google Talk, you're only going to be talking to other Googlers. And now that everybody knows Google is considered harmful, the only people left on this service are people who don't get the memo. Probably because they are slow, or dumb, or both. For realtime chat, use IRC. IRC was used during the [fall of the Soviet Union](http://en.wikipedia.org/wiki/Internet_Relay_Chat) to broadcast messages the goverment didn't want to get out. So, anyway, it's got a long and storied history of being reliable in tough political circumstances -- such as the current one.

Email is, as it always has been, much harder. You need to be an astro-GNUist to be able to run your own email server. At the point of this writing, I have not been able to figure it out. And I'm pretty smart. Unlike people still on Google. I can't figure out if it's a fundamental problem with email, and we need to do away with email altogether or man up and learn to set up fifteen different services with different config files -- let alone go back to configuring pop3 for people who can't figure this out. Everybody knows if you can't figure a mail server, you are not an idiot. It's just really fucking hard to learn these things.

For social networking, well, you probably should use IRC. As far as I can tell, social networks are all doing massive global censorship of topics people don't want to hear about. This started when my Bitcoin pieces stopped showing up in other people's streams more than a year ago -- prompting my quick and complete exit from using Google forever. If you are using Facebook, you are fucking yourself in a terrible manipulative environment. If you are using Twitter, you are talking to an empty list of people who are no longer on that service. Because no one cares anymore.

What else is there? Google Drive? For real?

Google Reader is gone. Analytics is useless. No one but spammers have used Blogger since 2006. The search engine is crap. If you use a Chromebook, you should put Linux on it and place a sticker over the logo. If you need a map, get out a paper one. Or walk around, and use your eyeballs.

Finally, I want to encourage you to get on Arch Linux and type 'sudo pacman -S youtube-dl' and download as many great Youtube videos to your computer as possible. Then upload them to your own Mediagoblin instance. 

Why? Because everybody knows that Google is considered harmful.

There are only two [talented](http://plan99.net/~mike/) [people](https://www.imperialviolet.org/) I know who work at Google. They should both have the balls to quit.

