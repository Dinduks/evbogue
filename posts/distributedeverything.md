---
title: Distributed Everything
date: 2013-06-26
---

*Please note: this was originally posted to Medium, then I deleted my account, and it was [preserved here](http://cache.preserve.io/jc5kv614/index.html). Now I've reposted it to evbogue.com*

### Why Medium isn’t the answer and how to use the Internet

Every morning I use a stove top espresso maker to make my coffee. I put three spoonfuls of Café Bustelo into the middle chamber, fill the base with water, and put the stove on low. Two or so minutes later I have some coffee.

I finally got my invite to Medium. No doubt because hype has died down, and Medium needs a new batch of numbskulls to get excited about front-end editability.

I’m more interested in using Medium to tell you not to use Medium. I wonder if doing this will get me banned from Medium, I suspect it will. Hence the problem.

In the beginning, the Internet was distributed.

My stovetop espresso maker is distributed.

In the beginning the Internet was distributed. Just like my stove top espresso maker.

Occasionally I go to Starbucks to get a cup of coffee. It’s easier this way. They have the coffee made for me. All I have to do is fork over $2.40 and they’ll hand me a cup of coffee.

If Starbucks was free, you’d get a cup of coffee with an advertisement on the side. And you’d go there more often.

Perhaps at the Starbucks of the future you’ll get a free coffee if you look at an advertisement for 30 seconds.

Starbucks is a centralized source of coffee. This means the system works the same everywhere you go. This is comfortable, because I know anywhere I am in the world. Whether I’m buying coffee in Toyko, New York City, or a suburb west of San Francisco I’ll get a very similar taste.

Not even five years ago, baristas in Starbucks pulled their own shots. But if you watch them do their work now, you’ll see this isn’t true anymore. A Starbucks barista is basically a button pusher. They choose which button to push, and push it. The drink comes out.

It takes zero skill to be a Starbucks barista. Sorry baristas, but it’s true.

Over the past five years, more and more people have started using the Internet like Starbucks. They’re all going to centralized sources for their cups of coffee.

You know the sources. You’re all on them. Facecrack. Shitter. To some extent Poogle Glus. When we think of the Internet, we think of these sources.

Now Medium wants to be another player. It wants you to feel safe to publish here.

When you use these services, you’re not even a button pushing Starbucks at Barista. You’re just ordering a free latte at hypothetical ad-sponsored Starbucks of the future. You’re not even pushing the button.You’re just a user.

So we have this generation of people who think using the Internet is typing into a box and pushing the send button on one of these centralized services.

Then this week the news comes to light that the biggest companies in America are also just funnels of information to America’s NSA. Nothing you do on them is secure or private.

These services are easy and free, but it’s also a double-edged sword because you’re essentially piping all of your private information straight to the United States government without them even needing a warrant.

### Get all mad and do nothing

People are mad about the fact that their favorite services are secretly handing over all of their data.

Most are doing absolutely nothing about it.

A year ago (late 2011/2012) I got very interested in this centralized/distributed problem. I’d been traveling around the world. First I went down to Mexico, and saw how free it is down there. I went back up to San Francisco, and I sold my iPhone in the Mission. I got &#36;360 for it, at the time. iPhones were still hot shit back then. The extra inch hadn’t been added yet.

Then I caught a flight to Singapore.

At the time I was writing a lot, and I didn’t know anything about Singapore. So I figured, why not head over there? Little did I know that Singapore doesn’t have the same freedom of speech laws we take for granted in America.

In other words, you can get in big trouble in Singapore just for saying or writing something.A few weeks ago, this became even more clear to everyone with the Singaporean $30,000 blogging license story I read a few weeks ago.

I suspect at some point the Singapore intelligence agency may have hacked into my email while I was there (another IP was registering on my Gmail account). Also I believe I was interviewed by a Singaporean spy.

Reading all of this, you might think I was doing something wrong. I wasn’t. I was just a tourist in Singapore writing to the Internet every single day, because writing to the Internet every day was my job.

This, in Singapore, makes you a suspect.

After the encounter with the spy in Singapore, I decided to leave. I booked a flight to Tokyo and 24 hours later I felt a lot safer.
### Centralized vs Distributed

After my Singapore trip, I started to get really interested in the centralized vs distributed problem.

Over the next few months, I traveled from Tokyo to Berlin to Kansas City to Boulder, Co. Everywhere I was seeing this problem, everywhere this problem was a part of me.

I’d deleted my website, and was using Google+ as my main publishing space. This turned out to be a bad idea, because I was getting more into Bitcoin.

Bitcoin is apparently a keyword that gets your posts sorted out of the stream In Google+. In Google’s world, ‘Caturday’ posts go viral. Bitcoin posts don’t even show up in your follower’s streams.

But there’s no way to prove this, is there? Because Google+ is centralized. It’s also closed source. So there’s no way you can see what is getting sorted out, and what isn’t.

Six months later, I had this same problem in New York using Mailchimp’s Tiny Letter. This time, I knew I was being censored. I wrote a piece to my list of 1600 people at the time with the word Bitcoin in it. Five minutes later I had an email in my inbox saying my message had been flagged.

After a day of emailing back and forth with Tiny Letter, I managed to convince “Katherine” that Bitcoin was a technology used by hundreds of thousands of people on the Internet.

She let me send that one message, but told me I’d end up in the filter every single time if I ever used the word Bitcoin again.

So, being that I wanted to continue to send more emails, I stopped using the word Bitcoin in emails.

### Censored

In the beginning the Internet was distributed. It was hard to censor people during that time. This is why the early Internet was so filled with furry porn. No one could stop you, because no one knew who anyone was.

There’s nothing illegal about posting images of cartoon animals having sex. It’s just weird. And Facecrack doesn’t want any weird. The only drama Facecrack wants is your relationship drama. Elementary-level ‘he said, she said’ drama.

So as more and more people started using these services, I noticed the level of the conversations around me dropping closer to the kind of conversations people have in kindergarten.

We’ve all moved towards these centralized sources because they’re clean, they’re easy, you know what to expect. Nothing jumps out of the woods at you. You don’t accidentally end up inside someone’s public furry porn collection, because the big companies ban this stuff.

Everywhere we’re starting to see report buttons. If you see something, say something. Furry porn? Report button. Bitcoin? Report button. Someone has some crazy ideas you don’t want to hear? Report button.

What does this sound like?

So now George Orwell’s still-not-banned book 1984 is flying off the shelves. And yet we’re all still Facecracking.

Why? Because we don’t know how to go back to the distributed Internet.

If Starbucks replaced all of the button-pushing espresso machines with real espresso machines, they’d have to fire everyone to works in Starbucks and hire baristas from Cafe Vita, Blue Bottle, and Stumptown.

Because no one who works at Starbucks knows how to pull a shot anymore.

The same is true for you. You have no idea how to type HTML anymore. You have no idea how to deploy a Node.js application. You have no idea how to create a link to another website.

If you can’t figure out how to make a link to another site without using a button to do it, how the hell are you going to figure out how to create a new Internet where all of the packets are encrypted?

How are you going to figure out distributed peer to peer social networking?

How are you going to figure out how you make all of your instant messaging forward-secure?

So my argument is this: don’t use Medium, because it makes you dumber. Go learn to pull your own shots. Get off these centralized services.

Don’t just log out. Delete your accounts. This will force you to learn how to use the Internet the hard way.

### Distributed everything

So I’ve illustrated the problem. Now I want to talk about a few of the solutions I’ve discovered in the past few years of pulling my own shots.

+ CJDNS + Hyperboria. There are around 100 people living and working on another Internet called Hyperboria. It’s based on a mesh networking router called CJDNS. If you get someone to peer you in, you’ll discover another Internet that’s a whole lot freer than this one.
+ IRC. Everyone who wants to make sure their messages land use IRC as a backchannel now.
+ Duckduckgo. The search engine that gets you out of your filter bubble, and respects your privacy.
+ Pump.io. The creator of Identi.ca has a new federated social network called Pump. Visit him at e14n.com
+ Host your own web server. Get your own VPS (Digital Ocean is only $5 a month) and host your own web server. Deploy using Node.js using Bitters.

When in doubt, start by learning some Internet basics. HTML5 and CSS3 are a good start for anyone who’s never learned how to code. Then move on to JavaScript and the other favorite languages of the Internet.

We’re not going to get through this by pushing buttons on centralized services.

If you want to use the Internet in 2013, you need to pick up some tech skills. In a few years this will probably be all figured out and no one will be intercepting your iMessages.

Until then, let’s send everyone a strong message by deleting our accounts on centralized services and learning how to use the distributed Internet.
