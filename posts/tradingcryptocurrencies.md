---
title: Trading cryptocurrencies
date: 2014-02-23
---

**Disclaimer** This is not financial advice, and you shouldn't find a way to misconstrue it in your brain to be such. I am not advocating that you should lose all of your precious money day trading Dogecoins. This is meant as an introduction and how-to guide on how to do it. 

Don't trade anything you aren't willing to lose. Duh.

***

Trading between cryptocurrencies is an often overlooked skill that no one talks about. Too often all we read is stories about how low or high the Bitcoin price is, or how hard Mt Gox has fallen and how many idiots left all of their Bitcoins on Mt Gox's servers.

But too little is said about turning your BTC into DOGE and back again.

The reason you might want to learn how to do this is someday a new Altcoin will come out that kicks Bitcoin's ass. And when that does, you'll want to invest in it early, instead of waiting until everyone else has invested and you have to spend your life-Bitcoin-savings to get three Altcoins.

A good way to practice for this inevitable moment is to learn to trade cryptocurrencies right now.

Here's how you can learn how to do that.

**1. Find a trading platform you trust**

Do some research on cryptocurrency exchanges and/or trading platforms. I won't recommend one, because you have to make the decision for yourself. Using a trading platform is an exercise of trust, because they might walk off with your wallet while it's on their servers. 

Here's a [list of trading platforms and exchanges](https://en.bitcoin.it/wiki/Category:Exchanges) on the Bitcoin wiki.

Another way to find an exchange is to check the website of the cryptocurreny you want to trade for. They will have more information on what exchanges support their currency. Every exchange will probably support Bitcoin, but not all will let you trade for Dogecoins.

I've been using Vircurex, but who knows if I can trust them or not. Do your own research and settle on an exchange that you trust!

**2. Load the exchange up with a small amount of Bitcoins**

Send a small amount of BTC to your exchange. They'll give you an address to send your BTC (or other altcoin) to. Make sure they arrive and are tradable before you send your life savings. Actually, don't ever send your life savings. Keep it in a safe place! Do a little research first to make sure you can withdraw your coins once you've traded. Some exchanges make you go through an extensive verification process before they let you do anything -- this is a headache, and usually related to them supporting USD/EURO to BTC trades.  

I have around 10,000 Dogecoins I'm playing around with right now. This is around $12 USD. I've managed to grow this to 11,113 Dogecoins via trading in the past few weeks. 

I realize I'm not making a fortune, but it's my intention to learn, not gamble massive amounts of cryptocash.

**3. Trade BTC for...?**

Depending on what exchange you use, your interface will be different. 

Here's is mine on Vircurex when I'm placing an order to buy BTC with DOGE.

<img src="/images/vircurex.png" class="profile full">

In this photo I'm placing an order to sell DOGE at .00000270 BTC when the price spikes again and it hits that number. Right now the price has been pretty low, so I'm all stocked up on DOGE. 

Now, it might be true that Dogecoin price never spikes again against BTC. If this is the case, then I'm a shitty trader (this is quite possible). But that's the risk you play trading cryptocoins.

**4. Watch the prices**

Now all you have to do is watch the various cryptocoin prices move against each other, and place appropriote orders when you figure you can make a profit.

It should be noted that all cryptocurrencies are an experiment. It's probably best to play with small amounts, and not take out a second mortgage on your home to try to 'win it big' on the markets. However, it can be fun to learn how to trade cryptocurrencies, even if you end up losing money over time. Which is what most people do.

-Ev
