---
title: Code elegance over user convenience
date: 2013-12-11
---

Why am I obsessed with [Arch Linux](http://arch.evbogue.com/) right now?

And more important, **why don't you get it?**

Have I not made the case for Arch Linux? Is it that [dwm](http://dwm.suckless.org) and [Terminology](http://enlightenment.org/p.php?p=about/terminology&l=en) don't capture very well in screen shots? Is it because these pieces of software don't have their own marketing departments? 

Is it that you believe a computer is there to distract you from your life, not enhance it?

I'm obsessed with Arch Linux for the same reason all of my stuff fits into one bag. I want as little as possible keeping me from doing the work.

I want to have the freedom to explore the world, board jets while I still can. The easiest way I've found to have this freedom is to not own a bunch of shit.

I pack everything down into my single bag. I've had the same bag for two years. The wear doesn't show, because it's from Mission Workshop in San Francisco.

Which brings me to another point. Sometimes it's worth it to do more, or spend more, to have a better experience. It seems outrageous to have spent $200 USD on a bag, but it's the best bag in the world. And I live out a bag. It seems outrageous to spend a few hours (or a few days, at most!) to get an operating system on your computer that is better than the one you have right now. Similar to how it would only take you a few hours to pick a few things to throw in a bag, and leave for an exotic locale.

But it's not that easy, is it? You want a nice computer showroom. You want to make a purchase. You want your computer software to have a charismatic/badass leader. Richard Stallman isn't badass enough for you? Linus Torvalds isn't charismatic enough for you?

No one is going to sell you on a better thing. To get a Mission Workshop bag, you have to avoid all the crackheads and heroin addicts as you wander down a dark alley off of 16th in San Francisco. Even if you take this journey on a sunny day in September, you still have to pay the price of admission to get a bag. No one is going to give you the best bag on the planet for free.

But the point of this piece isn't backpacks, it's **code elegance** over user convenience. That's a [core philosophy of Arch Linux](https://wiki.archlinux.org/index.php/The_Arch_Way). It’s why the Arch Linux USB installer boots your computer boots to a blank command prompt.

So maybe you're still into user convenience over code elegance. Most newbs are. Perhaps you only recently purchased a computer, and the shiny bubbly interface is all that you can understand or comprehend. This is understandable. Many, if not most, people are in your position. 

I, on the other hand, have been using computers for a long time. When I was born, there was a computer in the house. And this computer booted to a blank black screen, with a blinking green cursor. 

Of course, computers have come a long way since then. I'm not asking you to go back in time when you [install Arch Linux](http://arch.evbogue.com) on your system. Any software you're used to using on your operating system of choice is also available on Arch Linux. And you might just find once you use a tiling window manager, you won't be able to go back to clicking and dragging all of the time. I wasn't.

Or perhaps you want to stay with convenience over elegance. In that case, you'd best be on your way. We won't agree on much.

If you do agree, drop me an email. And answer the question: what's your stack?  

My email is [ev@evbogue.com](mailto:ev@evbogue.com)


