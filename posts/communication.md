---
title: Secure and distributed communication
published: 2013-11-02
---

It's unclear how to communicate in September of 2013. In the post-NSA-scandal world, we all know better than to think our unencrypted emails go anywhere other than the Utah data center. With all major digital companies backdoored by the NSA, it's also not any safer using an internal private messaging system either.

Meanwhile, we haven't yet figured out a distributed peer to peer social network with reliable and real-time service.

We still have a world-wide-Internet, and we can still communicate over it. But how?

Let's break these into two separate problems.

**Secure**

My approach to secure communication at the end of 2013 is 'take a walk'. Literally. 

If you want to talk frankly with someone about something, leave your backdoored cellular phone at home and take a walk with said person. The reason I say take a walk is it's easier to not be recorded if you're walking around the city. Also, you'll get some exercise at the same time. While you're on this walk, you can exchange private keys and be able to email each other using PGP. But will you? Probably not.

For secure-er email, I've been using a mail server within Hyperboria (ev@hyperboria.ca)[mailto:ev@hyperboria.ca], configured to send and receive packets within the cjdns network and the outside world. This means if you want to contact me there, over cjdns, your packets will be secure over the network. No man-in-the-middle attacks! But if you email me via clearnet, expect your packets to be sniffed on way in.

This is the state of secure communication at end of 2013. If you need to send a private message, take a walk. It may be your only hope.

**Distribution**

This is the other, more challenging, problem. 

If you want to reach a whole bunch of people in the post-NSA-scandal world, how do you do it? Definitely not via a centralized social network. Why? Because while it hasn't been proven, it's pretty clear all of the major ones are receiving NSLs to [gaslight](https://en.wikipedia.org/wiki/Gaslighting) their customers. This means if you send a message over a centralized social network, chances are it will only land if the contents of your message is 'approved' to land. 

This is why you see everyone going back to hosting their own content on their own websites. Because if you're using an IP address, with a web server, disseminating information across the Internet there is no possible way in hell you will be gaslit by the company you've trusted to distribute your content for you.

So how do you distribute content over the web?

One way you can do this is by using a Git repository. Check all of your content into Git, and because Git won't let you take anything away (all changes are hashed to a merkle tree since the first commit) there's little chance someone will tamper with what you're saying. 

Also, when you use Git to maintain your website, no one can inject malicious code into your site. Great! I've been storing my website content in a git repo since the beginning of 2013, and so far it's worked out very well. 

But this leaves the question of notification. How do you notify people when there is new content? 

As far as I can tell, there's no good way to do this right now. I've been using a mass-email provider to send messages, but I do this with a lot of hesitation because while my open rate is 70% better than the industry average, I also tend to write about things that get me 'flagged' in their system. 

This means my account gets suspended for a day or two, and I have to talk to their support desk to let me send the message. I've avoided problems by keeping my messages using this mass email provider short, and put the contents of my messages on my own web server. This prevents my message from getting caught in the rabblerouser filter, but isn't a great long-term solution. I can always be suspended for what I say on my own website, not what I say in the email message itself.

So how am I possibly going to have free speech and also reach people at the end of 2013?

One possible solution is to get a lot of you onto IRC. Internet relay chat is a great way to do realtime communication in a chatroom setting. On Hyperboria, it's one of the most common ways for people to communicate around relevant issues without being censored or gaslit. 

Install an IRC client. I use [irssi](http://quadpoint.org/articles/irssi/). /connect irc.freenode.net and /join #bitters. While this is named after the Node.js project I use for my website, it's also a good place to communicate about anything you want with your fellow humans during times like these.


