---
title: Gmail embargo followup
date: 2014-02-15
---

It's been an interesting week following my decision to embargo all emails to and from Gmail starting March 1st 2014.

In the first few days, I received no pushback to this embergo. A few people encouraged me to embargo all collaborators with the NSA. I may consider doing this in the future.

Tim wrote a piece asking me to '[expect better](http://fractaldragon.net/posts/2014-02-09-expect_better.html)' of Google. I disagree. We have given Google more than enough of our information, and even if it is true that they are driven by profits alone, then people who use Google are sheep in a meatgrinder of human capital. 

I'll go as far to argue that Google would still have the respect of its users if it had come out strong against the NSA. Now it's lost that respect. Google profits from sentiment alone, and as sentiment fades into the negative Google has a hard fall ahead of it.

It's time to take a hard stance on Google, and that stance is "not even once."

As I wrote a months ago, one of my biggest mistakes was using [Google+ as my website](http://evbogue.com/mistakes/). If you are using Google as your primary or only providor, you are bound to be fucked if you haven't already been.

#### Alternatives?

A handful of my readers quickly switched from Gmail to Outlook or Yahoo mail. It wasn't my intention to encourage anyone to switch from one centralized provider to another. If all you can muster is the energy for a few clicks, then by all means, you are welcome to use Outlook until I embargo Microsoft.

However, you might find switching to an alternative is more useful. No email is secure, but switching to an independent provider might give you more freedoms. If not more tech skills. If you're very hardcore, why not set up your own mail server?

#### Bitmessage?

One of the surprising things to come out of the past week has been Bitmessage. I've added a Bitmessage address to my sidebar, and will be using it to talk to anyone who Bitmessages me there. 

Bitmessage is...

1. Easy to install
2. May be secure
3. Innovative

While it's not certain whether Bitmessage will replace email, it is reassuring to know that there are other ways to talk to people with (possible) privacy. I'm interested in hearing people's arguements for why Bitmessage won't work, or if there are any technical flaws to it. But for now, it appears to check out for me.

Also, the easy-to-install thing is a big bonus. I've had some pretty self-described tech-unsavvy people get in touch with me on Bitmessage.

If you're on my list, please email me with an non-gmail email address. If you have a Gmail address I will be emailing you at some point soon to talk about your options. If you don't respond to my email, I will assume you don't use Gmail anymore and delete your email from my list.



