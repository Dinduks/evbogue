---
title: Free money advice
published: 2013-10-19
---

A lot of people ask me for money advice. I try not to write too much about it, because I'm not rich. I've had my moments of bringing in large sums of money ($20,000 weekends) and sometimes I've completely bottomed out during an experiment in the "gift economy" (Ev Bogue begging for money.)

In other words, you shouldn't listen to my money advice. It's probably very wrong. But most money advice is, so why not read mine and see what you think?

Most people giving you money advice probably don't know what they're talking about. Especially parents, grandparents, and friends working on PHDs who are underwater on mortgages. If they're rich, then listen to them. But if your parents/grandparents are underwater on two-three mortgages each, they probably aren't the best people to ask for money advice. 

Everyone will give you money advice, if you ask for it. 

Most people will give you advice on almost anything, if you ask for it. 

Maybe my best advice is not to ask for advice. Just do what you want with your life.

This being said, here are a few free rules of freedom, success, money from your host Ev Bogue.

1. **You cannot pay a mortgage by skipping coffee**. Coffee is one of the cheapest things you can buy. Standing around thinking "do I have enough money for this coffee" is one of the most expensive ways to waste your time. Just get the fucking coffee. Drink it. Think about how to make more money. I can't tell you how many people I've met in my life who waffle about for 35 minutes trying to figure out if they have $1.75 to buy a tall coffee at Starbucks just because they don't happen to have a day job at the moment. 
2. **You cannot pay a mortgage by preparing all of your own meals**. Do the math. Even if you eat out every single night for the rest of your life, it will probably not equal your mortgage. The lesson here is: very large price-tag items have the ability to destroy your financial sanity for the rest of your life. Whereas debating whether or not you have money to go out to lunch/dinner is destroying your financial sanity right now, and it's also not digging you out of debt.
3. **Know what you want**. I know myself. From many many experiments moving to many cities and trying out different lifestyles. I am most content when I walk around the streets of San Francisco muttering to myself with a coffee in my hand. I love burritos. I love living in neighborhoods just on the fringe of gentrification. I don't want to get tied down into long expensive obligations. 
4. **Cars + mortgages are the two single most expensive mistakes you can make**. I have never owned a car. I have never owned a house. Gone are the days of your boomer parents using their mortgages as piggy banks. Bill Clinton is no longer president. Avoid these two mistakes to avoid a lot of stress. Walking everywhere and living in a city with public transportation can remove much financial strain on your life. Also, these cities trend towards having more culture than the ones where everyone drives.
5. **Live in a shitty neighborhood**. There's nothing better than living in Williamsburg Brooklyn in 2005, or The Mission in San Francisco in 2010. But trying to live there AFTER it gentrifies is an economic nightmare. Get into hip/cool neighborhoods before old rich people and trust-fund baby hipsters find out about them, get cheap rent! "But it's DANGEROUS". To that I say: "I have only been almost-killed in well-to-do neighborhoods."
6. **Earn more**. Don't waste all of your time thinking about saving. Just avoid dumb expensive mistakes. Don't get married. Don't get a car. Don't buy a house. Now, focus on earning more. 
7. **Technical skills make you more valuable**. This is self-explanatory, but I'll explain it anyway. If no one wants to pay you for anything, it's probably because you never [learned HTML](http://design.evbogue.com), and you think posting a message on a social network means you are doing work. Just because you know how to power on your Macintosh, and fire up Microsoft Word doesn't mean you have technical skills. You're an idiot if you think having an iPhone makes you a technologist.
8. **It's best to not have a phone**. I haven't had a phone for a year and a half. I'm not saying a personal mesh device wouldn't be fun to have, but a closed-source phone is only a distraction. The only reason you take it out of your pocket so much is to justify the price. Do the math, it's not a cheap device to own. Get a shitty PC instead and [Deploy Arch](http://arch.evbogue.com) to it. The recurring cost of a shitty computer is $0.
9. **Yolo**. Take all of your money cues from rappers. You only live once. If renting a mansion and a limo for your music video is what you want to do with your time on this planet, then do it. Renting a mansion for a day is almost certain to be less expensive than getting a mortgage on a shitty house on the outskirts of a city somewhere to be trapped for the rest of your life as you waste away seventeen miles from the nearest coffee shop. The good news is, you won't have to worry about whether or not you can afford the coffee.

This, above, is my money advice. It won't make you rich. It won't make you famous. But it's guaranteed to be much better than the advice your parents, or that one money blogger who is underwater on an apartment building and/or RV, will give you.

If you have more money questions, send Ev emails [ev@evbogue.com](mailto:ev@evbogue.com)




