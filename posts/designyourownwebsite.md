---
title: Design your own website
date: 2014-03-16
---

This is the blog post where I tell you that you don't know how to code a website. Either because you never learned, or you're so rusty from ten years of typing into little blue text boxes that you have no idea how to type anything with brackets.

But I won't. Because I know you're very sensitive and fragile. Your ego might feel huge from all of the self-aggrandizing algorithms you've been sucking pipe on for the past ten years. Now that you're off the algorithmic junk, you're left with nothing but an empty hobo cup of useless muscles you over-developed while trying to get everyone to "Like" you. So the first thing you try to lean on is a couple of speculative SEO posts you read from some bullshitting motherfucker you know is wrong, but you tell yourself he's right because if you told yourself the truth you'd have so little to stand on that you'd have to cry yourself to sleep tonight.

Reality is to build on the Internet you need some solid skills. Solid technical skills. People don't get ahead on the Internet with witchcraft and bone dice, they get ahead by understanding how advanced, open, and distributed systems work.

One of the best advanced open distributed systems you can wrap your head around is how to put up a website. To do this, you will need to learn how to code "simple" HTML5 and CSS3.

This is why I wrote [Design Your Website](http://design.evbogue.com). Because there's no better place to start than with the basics. Once you have the basics, you'll be standing on a patch of solid digital ground.

 
