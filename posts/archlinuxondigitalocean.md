---
title: Arch Linux on Digital Ocean
date: 2014-05-07
---

**Update** *Digital Ocean will be depricating Arch Linux, so I will probably have to find a new provider. I'll write more when I do. Send recommendations.*

For the past year I've been recommending Digital Ocean as a VPS provider. I've also used Digital Ocean as my VPS provider for the past year.

In the first week of May 2014 I discovered Digital Ocean has decided to depricate Arch Linux as a standard Linux distribution for their droplets.

As far as I can tell, they've decided to stop supporting Arch Linux because they cannot figure out how to create a new Arch Linux image with the correct ethernet interface using QEMU. As far as I can tell, they cannot figure this out because they don't have any competent technicians on their team who know how to work with Arch Linux.

It's obvious Arch Linux is a major distribution used by many people. You only have to wander to Arch's IRC channel to see more than 1,000 users. All of Arch's packages are updated, and when they are out of date they are updated quickly. There are also a number of up-and-coming distribution based on the Arch philosophy. 

To me, it seems as that Digital Ocean cannot support Arch because they do not have the technical skills to support re-imaging Arch for their clients.

As for clients who use Arch Linux, I think Digital Ocean should support Arch the same way the Arch Linux community supports Arch. 

"If there is a problem with your system, it's your job to fix it."

In the case of Digital Ocean, if there is a problem with how their system interfaces with Arch Linux, then they need to fix it.

I got in touch with Digital Ocean's support team to give them an opportunity to fix this, and not depricate Arch. We are having a conversation about this problem now. 

If Digital Ocean does depricate Arch, I will be forced to unrecommend them as a VPS host and they will lose me as a client.

If anyone else who reads this blog also uses Arch Linux on Digital Ocean then now is the time to reach out to them and express your needs, before it's too late and you're stuck with less than awesome distributions on their service.

