---
title: Cryptocurrencies
published: 2013-10-11
---

At the current moment, I have 1 BTC in my bag. It's a physical Bitcoin, and the only Bitcoin I possess. Since there's another huge surge in the value of Bitcoin right now, I thought I'd take a moment to express my experience with it, and how I see the future of Bitcoin.

I first heard about Bitcoin in late 2011, while I was staying in a hotel in Mexico. I read this piece in Wired, and exclaimed outloud 'holy shit, this is going to cause the government to collapse.' I haven't been right about that yet. 

I got really into Bitcoin for the next year. I followed all of the Bitcoin startups through 2012. I talked to a lot of people about the cryptocurrency. I got very good at explaining how Bitcoin worked. I even accepted Bitcoin donations and payments for my digital products using Bitcoin.

I made a little chunk of Bitcoins over the course of 2012. Around 12 BTC in total.

Then, in early 2013, during the huge price spike, somehow someone was able to either guess my password on Blockchain.info, bruteforce the account, or perhaps Blockchain.info randomly clears out accounts (I have no idea if this is possible, and have no proof if this did in fact happen.) Regardless, I lost the 12 digital Bitcoins I was storing in an online wallet.

This doesn't seem like much Bitcoin to lose. It wasn't. But it was enough to temper my enthusiasm for Bitcoin. My initial enthusiasm transformed into silent skepticism. 

I do believe cryptocurrencies can become the future. However, I'm not betting on Bitcoin being the one cryptocurrency that wins. Why?

1. Security. It's stressful to hold Bitcoins, because you're never sure how someone is going to 'get them' from you. I found having even as little as 12 Bitcoins was stressful for me, because I kept checking to see if they were 'still there'. I know, I should have been keeping them air-gapped from my computer in a wallet file. However, the idea of air-gapping my Bitcoins and hoping my wallet file was still there when I came back was also stressful. What if I couldn't get access to them again? And I'm a pretty tech savvy dude. The cryptocurrency that makes it secure and easy to store value will ultimately destroy Bitcoin, because anyone will be able to use them and keep them safe at the same time.
2. Anonymity. Bitcoin isn't anonymous. It's been interesting to see how people in the Bitcoin scene have been able to identify transactions belonging to specific individuals and expose them for all to see. A great example of this is the captured Bitcoins from Silk Road. Any cryptocurrency that guarantees anonymity will destroy Bitcoin.

I'm also concerned that the United States might not be the best country to hold Bitcoins or any sort of 'edgy' currency within. It's clear the United States government is hellbent on curtailing it's citizen's privacy making sure they stick with their authoritarian system. This is too bad, because the United States used to be a pretty stellar example of a free country. But over the last twelve years it has turned into a the U.S.S.A.

One of the big reasons Bitcoin gained so much popularity is by using it as a tool to circumvent government freezes on journalist funds. Typically when the government doesn't want journalists or whistleblowers to continue publishing within the country or abroad, they order banks and payment companies to freeze the journalist's funds. We saw this with Wikileaks over the past few years. Not being able to accept payment or donations for work can be a huge hinderance for work. However, people who are being pursecuted and pursued by the US government usually have a lot of support from within the country for exposing the corruption of our government. This is the on-going elimination of privacy, and the general feeling of 'not feeling safe to express oneself' that comes from having been born into the once free, now authoritarian government that is the United Socialist States of America.

So maybe Cryptocurrencies are a hope for a freer world. When no one can freeze your funds, you're free to do your work without fear. Without the currency being in the hands of one person or government, you can trust the algorithm to keep the correct score.

I'm hopeful for Bitcoins, but I'm also aware of their flaws. 

The good news is, there's a tremendous diversity in new Blockchains, or forks, being fired up using different strategies than Bitcoin.

What I'm doing is keeping an eye out for cryptocurrencies that hope to solve Bitcoin's problems. These will be the cryptocurrencies I bet on.

In the meantime, if you hold Bitcoins and want to buy any of my products using them. Please give me a shout at [ev@evbogue.com](mailto:ev@evbogue.com)
  
