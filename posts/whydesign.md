---
title: Why I wrote Design Your Website
published: 2013-11-15
---

<a href="http://design.evbogue.com/"><img src="http://design.evbogue.com/images/designyourwebsite.jpg" class="profile" style="width:250px; float: right; margin-left: 1em; margin-bottom: 1em;"></a>

Earlier this year, when I was writing/releasing [Deploy Node](http://deploy.evbogue.com), I received some unexpected pushback. 

"This is too hard." was the undercurrent of the messages.

After a bit of confusion on my part, and asking a lot of questions, I realized I was writing to a generation of people who had never learned the basics of designing for the web. 

I've been designing for the web for a long time. I created my first website on Geocities at 12. There I hosted digital fish for a game I'd been playing. This evolved into early blogs in 1999, then working at Gawker and Nymag's blogging networks in New York in the mid-2000s. Finally I quit my job, and started traveling around the world. How was this possible? I had a website.

So earlier this year, I found myself quite befuddled that so many people don't know their basics. The ABCs of the web, per se. But it isn't your fault. To a large extent, I blame social networks. These terrible little pieces of cloud-hosted closed-source software give you the illusion of participating in the web, when you're really just a cog in someone else's sophisticated manipulation machine. You have control over a couple of input forms, and that's it. These social networks get to fuck with you the rest of the time.

Now that we know all of the social networks are just great ways for you to put all of your personal information in neat little tables and rows to be shuffled off to NSA-HQ, the open web has had a resurgence. Unlike on a social network, you can't censor a website without someone knowing about it.

So having a website, in 2013, is one of the most free and public forms of expression you can have. You get to call the shots on your website, no one gets to call them for you. You have control over your own domain. In 2013, it can be a minor revolutionary act to simply maintain control over your own website.

But yes, you will need to learn some HTML and CSS to get a website up and running. 

Simultaneously, I was looking around the web for a great resource to recommend to people who needed to get the basics down before they delved into harder stuff such as Node.js. I couldn't find any. While I had grown up with some excellent resources, many of these have since passed into the great beyond, or fallen into terrible disrepair. We're left with an Internet full of shitty over-designed web design sites showing you thousands of crap tricks you 'might' need to know, but if you did, who would care?

Meanwhile, the basics of web design appear to be lost to the public Internet at large. All the best resources are buried under heaps of untrustworthy shitstick websites.

Having no other options to refer you to, I decided to put together my own resource. 

The goal: to teach you the basics of web design and best practices at the same time. 

Design Your Website has been a work-in-progress for the past couple of months. I took my time with it. I wrote the articles when I felt inclined to do so. I waited until you had questions, and then filled in the blanks. 

And the [final product](http://design.evbogue.com), I can say with certainty, is everything you need to know to get up and running with a website on the world wide web. All killer, no filler.

