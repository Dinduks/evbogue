---
title: I don't send mass emails anymore
date: 2014-03-14
---

Last week I sent my final mass email. Then I deleted the account I had with Mailchimp.

I feel relieved. After almost a year of reluctant Mailchimp usage, being censored once (for saying the word "Bitcoin" in a Tinyletter email) and then kicked off Mailchimp once (for saying the word "Fuck" in an email) I just didn't know what was going to get me censored and what wasn't. So I ended up not expressing myself. According to the Mailchimp support staff there is a "secret list of keywords" that ISPs send around to censor people. They can't reveal what this list is, because if they did people would avoid the words when they are sending emails. 

This seems to me akin to secret courts and national security letters (and most of the words on the list presumably come from national security letters) so I just didn't want to be a part of it anymore. 

This situation is too bad, but I can see the other side of the coin. Email is pretty fucking broken. It's easy to grab a list of emails and then spam people, and there isn't much the recipient can do about being spammed. 

I exported the list of emails of people who read my website, and then put them into my Gitodex (email or Bitmessage me if you want to hear more about what this is.)

So what now? Well, if you want to be notified when I write new piece you'll need to figure out how to use Bitmessage. 35+ people have already figured out how to do this, so I'm hopeful the rest of you can muster the technocourage to use your brains to do the same as the first 35 brilliant early adopters.

Once you wrap your brain around Bitmessage, subscribe to my BM address in the sidebar and turn Bitmessage on for at least 15 minutes once per day to get broadcasts.

I'm sure some of you oldskool folks will beg me to implement RSS. I doubt I'll do this, because I just don't think anyone but a few fringe techno types still think RSS is the future -- RSS just doesn't have adoption amongst normal people or techno-extremists. Please send my condolences to Dave Winer, but RSS is dead. Most people use web browsers (such as Tor) to navigate to websites. 

As for direct p2p emails, I'll continue to send them out to you on occasion when I think about you. If you want to talk direct with me, you can either wait a long time for me to email you (it's a pretty large list of people to go through one by one) or you can email me. I'm happy to talk about anything, and respond to all emails. If you want more intelligent conversation and less harsh pushback from me, send me an eloquent argument or direct question -- do not send me five paragraphs of bullshit about your life story. I know your life is really important to you, but I probably don't care unless you happen to be Kanye West.

The inevitable question here is "how will you REACH PEOPLE?" says the horde of SEO(ahem, witchdoctor)specialists who are reading this. Well, to be quite frank, I already reach people. You're reading this, right? The simple answer is when I close some doors, others open. I'd rather talk to tech savvy people on Bitmessage than people who are clinging to their Pooglemail accounts like it's still 2004. 

You're ten years older now. It's time to change it up.

-Ev


