---
title: The Singularity is over there, somewhere
date: 2014-04-03
---

This morning, I believe it's a good time to turn our attention on the future. I'm drinking a coffee, and thinking about the future of futurism.

As many of you know, I've tried my hand at Great Futurist Texts before. After getting swept up in the gravitational pull of The Singularity, I started to see exponential curves everywhere and wrote a book called Augmented Humanity in early 2011. The book was a total failure (thank cyborg Jesus). It was a good thing I had a few thousand dollars stockpiled at the time, or it would have been a more spectacular failure than it was.

Anyway, let's talk about The Future of Futurism.

The truth of the matter is, we don't have good believable leaders in Futurism anymore. Ever since Arthur C. Clarke died, Ray K got that J.O.B that told him to sit down and shut up, and who even knows what happened to KK... Remember KK? 

The question I have to ask is this: what happens to the future when all of the most famous Futurists are sucking on Sergey Brin's tiny little weewee?

I, for one, think we should get a whole lot more skeptical about what the future holds. I think we should imagine that The Singularity is Over There, Somewhere, and not get too swept up in the hype of thinking that computers will be less dumb than humans in the near future. No matter how hard we want to predict this, my computer seems pretty dumb. Any intelligence manifested in machines in 2009 was Steve Jobs' ghost in the machine, now that we've [exorcised his ghost](http://evbogue.com/exorcism/) (and tossed our iWeewees in the nearest fish tank) we can see that our computers have returned to being tools. 

The modern day futurist is running Arch Linux, or Debian. A futurist knows his Vim commands. No futurist has a Poogle account or a Facecrack account and lies to themselves that their funny little human-controlled algorithms have a little glimmer of intelligence behind them. That glimmer of intelligence on the other end of the pipe you're sucking on is Zuck's milky tit.

If The Singularity really is Over There, Somewhere, we're going to have to do some work to get to it. Cyborg immortality is not going to show up one day and sweep you off your unsuspecting feet as you're sitting on the toilet scrolling with your poopy thumb. 

A futurist never takes his device into the bathroom with him. He knows any mobile device is being watched by the creep squad with the secret courts. Even turning it off doesn't do any good when you aren't willing to get into the bones of it.

Right now, even if The Singularity is rocketing towards us at Warp 9.5, at this rate it'll look more along the lines of the future from The Matrix. An evil race of giant robots enslaving the entire human population. Will those robots let Ray K live forever? Not unless he finds a way to get himself into a sexier package. A man who can only talk and boss engineers around is not going to be controlling an evil race of robots unless he can find his command line. 

Modern futurists dream of a day when they can evolve into a race of open source cyborgs. No futurist is willing to implant non-free software or hardware into his body. 

Imagine if the NSA could spy on all of your vitals. Imagine if the NSA could kill your eyesight if you say one thing bad about them.

"The NSA is a bunch of fuckwa...." "Fuck, why can't I see anymore?"

This is not the situation you want to be in in 20 years. If The Singularity is over there, somewhere, we'd best hope it isn't centralized. And if it is, we must fight back by not buying into the corporate bullshit, lest we install cyborg parts that turn us into owned entities of a giant corporation. Kind of like Ray K is now, for the giant pay check he's hoping will float him to immortality. Or pay for that storage locker with all of his papa's old crap in it.

If anyone is working towards a free and open evolution of the human species towards a race of super cyborgs, it's the folks at GNU. They have their priorities straight. No non-free software, ever, in cyborg parts. 

Right now we're psychic cyborgs. We augment our brains with software that has the potential to reach anyone the world. It's too bad most of you are stuffing your faces with government sanctioned propaganda on centralized social networks. 
 
Wasn't this the epic metaphysical battle between Captain Picard and The Borg during [TNG](/scifi)? The argument was humans used tools. The borg used humans. If humanity was to stay free, they'd best not be used by tools.

And there you are, poopy thumbing all day on a social network being used by a tool. This means you're a tool, because your brain is being used to generate revenue for some rich white motherfuckers in The Valley who want to see you suffer while they sniff boatloads of Columbian cocowana off the backs of young half-asian marketing department hookers.

What I'm saying is this: if you want to be a religious nutso believing that technology is going to liberate all of humanity, you'd best get your priorities in order. 

The only Singularity worth having is an open source one. 

Otherwise when The Singularity gets here, you are dead. The cylons killed you and used your remains to fuel their spaceships.




