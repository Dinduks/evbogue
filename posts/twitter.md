---
title: Ev Bogue Twitter
date: 2013-12-02
---

I do not have a Twitter account.

I do not use Twitter. At all. Anymore. If you want to get in touch with me, you need to use my [email address](mailto:ev@evbogue.com), or visit my [IRC](/irc) channel.

I used to have a Twitter account under the username of 'evbogue', but I deleted it in 2011. Someone squatted on the name, does not use it, and Twitter will not give me this namespace back. 

I don't care, because Twitter doesn't resemble the app you heard about in 2007-2010. What was once an amazing application with an open API to disseminate information is now a public company trying to squeeze its users for every bit of money they have. I don't want to be part of this, so I won't do business with Twitter. Twitter is not a good way to 'reach' people, if that was what you're after. 

For awhile I used an account at the username 'venportman', but I found it didn't benefit me at all to use Twitter, so I stopped.

If you are searching Google, it might auto-fill to 'ev bogue twitter'. This is because a lot of people use Google to find my Twitter.

But you shouldn't use Google to search for me. You know better, right? Use [Duck Duck Go](http://duckduckgo.com) to search for me. Then you will have private searches. Duck Duck Go's search bar doesn't think for you, so you can think for yourself. You do know how to think for yourself, don't you? 

Think for yourself and don't use Google or Twitter anymore. 

