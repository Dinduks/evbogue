---
title: Gwen Bell
date: 2013-11-13
---

I have some stories to tell you about Gwen Bell.

But I'm not going to tell you here. You have to [email me](mailto:ev@evbogue.com), and you have ask "tell me a story about Gwen Bell." And I will. I promise. 

Let me start with our 'professional' relationship. 

In late 2013, I produced Gwen Bell's book, [Align Your Website](http://align.gwenbell.com/). 

Gwen Bell, on occasion, is known to do merge requests when I spell things wrong on this website using Git. Because, as far as I've experienced, Gwen Bell is a [Git ninja](/dontknowshit).

We've been accused of being the same person. But I can assure you, Gwen Bell is not the same person as me. Though, we might be almost the same height and length. 

What do you want to know about us? [Tell me in an email](mailto:ev@evbogue.com). I'll answer your questions there, and perhaps I'll paste them here. 


