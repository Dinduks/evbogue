---
title: The 168 Hour Workweek
date: 2014-04-15
---

### Escape homelessness, live and work from a 3rd world country, and join the unlimited beer riche.

My brother emails me today. He's got his paramedics license and is reading the Four Hour Workweek. Eight years after it was published.

> I'm reading the four hour workweek right now. And am considering an attempt at doing what he advocates.
I am unsure if you've read the book, or know who he is. But considering that he's done something similar to what you started doing a few years back, I'd love some pointers from you.

Here are your pointers, Drew.

Let's start here... 4hWW is still on the bestselling selves of most <del>Borders</del> Barnes and Nobles. Think and Grow Rich is still a best seller too, and that book came out 80 or 90 years ago. As far as physical books go, there aren't many options besides these two books. I have to admit I've read each at least 3 times. However, it's 2014. We gotta update our mental models.

I listened to Timothy Ferris's The Four Hour Workweek sitting at a desk in 2007 in lower Manhattan. The audiobook. I'm not certain Tim Ferriss was reading, but maybe he did. Anyway, I thought it was a load of bullshit. Live and work from anywhere? What? I'm living and working from this fucking desk. This CANNOT be possible.

Geo-arbitrage? Working four hours a week? Throwing all of your shit in a backpack and living and working from anywhere? This CANNOT BE POSSIBLE.

After listening to the book on my iPod (they had those back then) I went outside and smoked a cigarette. My mind was a rush of questions. I was almost hallucinating with questions. 

Here are a few of the questions I had, that had to be answered.

1. Can I live and work from anywhere?
2. How is this possible?
3. How much of this book is bullshit?

And of course, many more.

The problem with a book of course is it is bricked. The Four Hour Workweek got an update in 2009 or something, but the majority of the book stayed the same. The trouble is the landscape of the Internet is different than it was in 2006. Think about it, Twitter hadn't even been invented in 2006! Google was a private company. Steve Jobs didn't even know he had cancer yet. The NSA wasn't watching your boobies on Skype. No one read Zerohedge. The economy hadn't even collapsed yet! 

Little did I know that a small part of the book would stick in my craw, and I would undertake a similar journey two years later.

In the fall of 2009, I threw all of my shit into three bags. One giant backpacking bag, one Brooklyn industries side-strap, and a camera bag. I threw out of the rest of my junk and jumped on a $211 one-way ticket to Portlandia -- the most exotic place I could think of going.

Little did I know this was the beginning of a grand new era. An era where I struggled to make money, then made $20,000 in one day, then ended up homeless on the streets of America over a mere four years of adventuring.

I ran into Tferr in the elevator at The Ace in early 2011. I was with Gwen Bell, but this was before I was bangin' Gwen Bell. He was shorter than expected, and gayer looking. Or the party he (and I) was coming from was gayer. So he probably thinks the same thing about me. Anyway, he pretended he didn't know me. I shook his hand, and said 'Hey, you're Tim Ferriss. I'm Ev Bogue, and this is Gwen Bell." That was it, then he got off the elevator and left, and we went up to Gwen Bell's room at the Ace where she painted my nails electric blue.

These days you'd be better off getting a hooker at an Airbnb than trying to book a room at the Ace, but this was 2011, you could still do that kind of thing.

Now that it's 2014, EIGHT YEARS after T4HWW was published I figure it's time to update the fucker. If Tmoney won't do it, I will. If you hate it, there's no need to throw a raccoon at me. The world has changed. It's time for us to change with it.

### Chapter 1. Seth Godin doesn't get speaking gigs anymore.

There's something about lying to your audience every fucking day that can throw a wrench into your wallet. It leads to some unexpected results, such as no one trusting you anymore. Lying isn't legit, bullshitting isn't legit, Seth Godin isn't legit anymore. Now I have no beef with Seth, but I wish he'd stop lying and write the following post.

"I'm broke. I'm scared. All marketers are liars, and I've been bullshitting you since 2011. In fact, all of my social proof is daily linkbots. The world has changed, people without tech skills don't have an edge, and I should have learned how to do geoarbitrage three years ago."

But he won't ever write that, because the problem with the great chasm of bullshit is once you're on the other side it is very hard to jump across to the right side of things.

These days it is very much impossible to reach anyone. If you want unlimited beer money, you best cold call a few motherfuckers. By cold call, I mean get in their inbox, with a personal message. Do not add your entire Peemail addressbook to Mailchimp and then spam people. Don't write about Bitcoin to Mailchimp or Tinyletter either, because they will censor you. No, the only way to make money on the Internet is to establish real trusted relationships with clients who want to learn things they don't already know. 

There is no such thing as something for nothing. Something only comes if you're willing to lock yourself in a room with seventeen beers and cold call the slippery-as-fuck nearly-anonymous people of the current Internet and attempt to convince them that you provide a service that will give them an edge.

Seth? He doesn't get speaking gigs anymore. Or book deals. I'm pretty broke, but I imagine he's broker. Until proven otherwise with cold hard facts. None of which are available anywhere.

### Chapter 2. Get your ass to a developing country.

There's a reason why I'm writing an entire book to you right now. The updated edition of the 4 hour work week. I mean, the 168 hour work week. 1. I have unlimited beer money thanks to Marco, Elina, and Samy who all bought Gwen Bell's latest book at http://gwenbell.com/ Thanks guys! 

Also there is one hell of a tropical storm in Mexico City. It must be an el nino year, because all advertising pointed to April being a throat-clogging pollution-ingesting, terrible time. This has not been the case at all. 

But, I have to pee. I'll be right back to finish this chapter.

Alright, I'm back.

Mexico City, and both of my Mexico experiences to date, have been far more enjoyable than living in los Estados Unidos over the past few years. Ditto for Japan and Singapore. There are no advantages to living in a 1st world country anymore. While no one at the IMF will recatagorize these countries, I want to.

Every developed country, with the possible exception of Russia is now in a new category. It is "Undeveloping". If you live in a country with oodles of homeless people and $2000 USD rent, then you qualify. The only reason Russia doesn't fit into this category anymore is they become an undeveloping country before everyone else. They collapsed into a pile of shit, and then came out the other side as the only country that can give you a budget flight to the international space station.

But where was I? Oh, you. You need to get your ass to a developing country right now. Because the beer is cheaper. And the apartments are cheaper. And the hookers are cheaper and cuter, and legal too. Not that I'd know. But if I did, I'd know legally. Because earning cashmoney with your sexy sexy body is very much legal here. Also killing yourself, that's legal too. Which is a big relief, because I know if all else goes to shit, at least I don't have to hang myself in a closet somewhere. 

Get yourself to a developing country. There's nothing you cannot buy here that you cannot get there and it's much more fun than waiting for an authoritariann regime to redistribute the wealth. Yay!

### Chapter 3. Learn another language.

You aren't worth shit on the Internet if you cannot program. If you can't program HTML5 and CSS3, today is a great opportunity to start. Buy [Design Your Website](http://design.evbogue.com), and be less of a waste. 

You have to be willing to fall on your face day after day to learn Haskell or Javascript or Spanish or anything worth learning. Your dopamine center will remain unlit. You will hate yourself, and everyone listening will laugh at you. This will be horrible. that's the point.

But no one lives and works from anywhere without doing some work. You're liable to end up homeless on the streets of unforgiving America if you can't learn a few things. And who knows, the dominant (whatever happened to dominatrixes?) language on the planet for the next hundred years might not be English. It might be programming. It might be Vulcan. Who knows. But you gotta start stretching your mind now.


### Chapter 4. Book that one-way plane ticket.

This chapter is brought to you by the crazy-fucking-tripping Flaming Lips album The Terror from 2013. Thank you Wayne Coyne.

Given that WWIII is a mere xxx days away, don't you think it's a good idea to get the fuck to a country that doesn't give a shit about pissing all over the world? Yes, it's a good idea. 

Besides the rent is cheaper. Not as cheap as living at yo momma's house since you were born, but cheaper. My rent is only [5000 pesos](http://evbogue.com/500pesos) per month. If you've never done a currency conversion, navigate your sorry ass to [coinmill.com](http://coinmill.com/) and start learning to think in different currencies. It's one of the most important skills an internationalized person can employ. It's the only thing you need to know about geoarbitrage. 

Are you going to make more money or less based on your current income? Given that your current income is probably rooted to your location, probably less everywhere. But we'll talk about that later.

Now navigate your ass to your favorite plane ticket buying platform and find a cheap one-way ticket to the closest developing country. You do have a passport, don't you? Why one-way and not round trip? Cause you're not going back. Just think about the nukes. 

For me, this tends to be Mexico. No, you can't sleep on my floor. Go find your own fucking accomodations. My 5000 peso apartment is not a homeless shelter. 

I am not your socialist bailout plan.

Motherfucker.

### 5. Fuck Minimalism

This is the point in the 168 hour workweek that you put all of your belongings into carry-on luggage. Why? Because the budget airline you booked charges you $75 to check a bag.

### 6. Uproot your income from your location

I realize it was the wimpiest shit possible to pick my first exotic destination of Portland Oregon at the end of 2009, but it helped me learn a valuable lesson. You cannot earn money in some places in the world. 

For example, the other day I bought 20 guppies for 50 pesos. Do the conversion. That is a bag of guppies for $3.50 USD. Now do the conversion into Bitcoin. I know, not a lot, right?

Well, I can't compete with Mexican wages. My grown-up-under-Bill-Clinton-mentality is too posh to handle hustling all day for 5 pesos. 

This is where the 168 hour workweek comes in. I gotta hustle online every moment of every day to make it. It's hard. It is the hardest thing I've ever done.

In order to live and work from anywhere you need to Do The Work. If you don't know how to Do The Work, read another brilliant personal development for non-bullshitters book The War of Art. 

Doing the work means keeping your head in the game every day. It means closing every sale. It means keeping both of your feet in reality. It means acknowledging when things are hard, or don't work out the way you planned. It means experimenting to cut through the bullshit. It means calling bullshit on [someone else](http://gwenbell.com) when they have the resistance. It means [developing your business](http://dev.evbogue.com). 

### 7. Unlimited beer money

Doing the work means drinking yourself to sleep every night. Why? Because you gotta feed the guppies. But also because you'll have earned it.

This is the last fucking thing I'm writing to this website for the next two weeks. Now go do something with your fucking life.

Ev Bogue





