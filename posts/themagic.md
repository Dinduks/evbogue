---
title: The Magic of Building Your Perfect Machine
date: 2014-01-23
---

*by [Gwen Bell](http://gwenbell.com)*

#### There's Magic in It

How old were you when you got your first machine? What was it?

Where did you live? What did you wear during that time? (Choose one or more, or create your own: Cordouroy. Flannel. Glasses. Braces.) Did you reaching it require a stack of books beneath your tush?

Was it a brand new machine or a borrowed one? Was it a hand-me-down? Did your parents get it for you? Had it been in the house since you were born?

What did that new machine mean for you? Did it give you freedom from an annoying brother? Was it a portal you could escape into when your whole family was being idiots? Did it mean you could take hours for yourself and not have to justify them to anyone? Was that first machine the one you played hundreds of hours of Sim City on? Tens of thousands of hours of ClanLord on?

Was your first machine a symbol of freedom?

***

[Build Your Perfect Machine](http://build.evbogue.com) won't take you back to those early days in your parents' basement. What it _will_ do is get you feeling those early feelings you felt with your machine: freedom top of the list. But also privacy. Aloneness. You won't feel as though you're being spied on with Arch Linux, unless you have other spyware in your room.

Like an annoying little brother.

It takes time to get it set up. But that's the point. It's not "power it on and it just works" because it's not. On purpose. The more time you spend with your box the more you discover the tiny joys of cacafire and other funny things you get to do on the command line.

Yes. I said get to do. Because if your time on your machine is a chore, a bore or an agonizing pain in the ass, it's probably because you're on the wrong box.

<a href="http://evbogue.fetchapp.com/sell/dyiwoove"><img src="http://build.evbogue.com/perfectmachine.png" style="float: right; width: 40%; margin-left: 1em; margin-bottom: 1em;" /></a>

Own, operate and maintain your own machine.

There's power in it.

But above all, there's magic in it.

Gwen Bell, <br />
January 2014 <br />

<a href="http://evbogue.fetchapp.com/sell/dyiwoove"><button>Buy Now | $27</button></a>

