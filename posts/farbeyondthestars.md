---
title: Do not trust messages from Farbeyondthestars.com
date: 2014-02-24
---

A reader notified me that someone has put a landing page up at Farbeyondthestars.com to capture email addresses and Bitmessage addresses.

I want to be clear *I do not own farbeyondthestars.com anymore*. Someone else is trying to build a website on this domain. 

There are three signs this is not my landing page. 

1. I do not use Aweber
2. I do not use website analytics
3. My web design skills aren't this crappy

For those who do not know, I used to blog at Farbeyondthestars.com from 2009-2010. I let the domain name go in 2012 because I no longer used it. I only own two domains at the moment, they are evbogue.com and venportman.com. Do not trust messages from Farbeyondthestars.com. Someone else owns this domain now.

On Bitmessage, only trust messages from the Bitmessage address on my website. I have the private key for this Bitmessage address secure and I won't Bitmessage you with another address unless we confirm through another channel or via my confirmed Bitmessage address.

When in doubt, email me at [ev@evbogue.com](mailto:ev@evbogue.com) if you have any questions about messages you receive. Especially if they claim to be from me.




