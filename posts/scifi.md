---
title: Ev Bogue's Ultimate Sci-Fi Companion
date: 2014-01-22
---

According to the Internet, a companion is a character who travels through space/time with Doctor Who. Knowing this to be a fact, consider this piece to be your campanion (or the little alien voice in your head) as you travel through space/time experiencing science fiction.

Gwen Bell sudo asked me to put this piece together because of my extensive experience with watching science fiction TV shows and movies. I haven't watched everything, but I have watched pretty much everything. My experience with reading science fiction is lesser. I'd almost always prefer reading a non-fiction book to a fiction. Science fiction looks best on your laptop screen, so why leave it up your imagination when you can watch it?

Science fiction is important for technologists because technologists interact with computers all day. And what are computers but creatures of science fiction?

### Television

#### TNG

The first television series I ever watched was probably Star Trek: The Next Generation. It was on once a week from 1987 to 1994, so I watched it from 2 years old on. I re-watched certain sections later in life to make sure I didn't miss anything as a toddler.

**What is it?** For those who haven't seen TNG, you might already know the premise. Captain Jean Luc Picard and the best crew in Starfleet boldly go where no man has gone before. This goes on for 7 seasons at 26 episodes a season for a total of 176 episodes.

**Highlights** When the Borg (a race of cybernetic lifeforms in pursuit of perfection) attempt to destroy Earth. I watched this two-part season-stretching heart-pounder many more times than once. The last episode wraps up the whole thing very well, and connects Picard back to his roots in the first episode.  

#### Doctor Who

Doctor Who wasn't on when I was a kid, but my dad had many of the Tom Baker (the guy with the super-long scarf)-era episodes on tape so I watched many of those. I then picked up the show again when Matt Smith was the Doctor, but only watched the Amy Pond seasons -- being driven insane by the annoying theater student The Matt Smith Doctor dragged around throughout 2013. I've only seen a tiny bit of the entirety of Doctor Who, but I'm happy to say it's the tiny bit I find enjoyable. 

**What is it?** The Doctor is a Timelord, a member of an ancient race of timetraveling aliens with two hearts. Depending on what time-period you're watching, the timelords have a homeworld, or it has been destroyed. The Doctor ends up being the last Timelord left in the universe. He develops a keen affection for a race of people on a distant planet called Earth. Throughout all of Doctor Who, The Doctor finds himself returning again and again to Earth to save it from one or more aliens trying to destroy it for various reasons.

**Highlights** Everyone has their own favorite episodes and Doctors. Me, I'm still partial from growing up watching Tom Baker, and enjoyed Matt Smith when he was traveling with Amy Pond.

#### Deep Space 9

As a teenager, I watched Star Trek: Deep Space 9 all the way through a couple times. It's one of the lesser known, and now almost completely forgotten Star Trek spin-off series. It's weirder, and more war-torn than any of the other Star Trek shows. There's a black captain, a shape-shifting chief of police, and of course Worf from TNG comes to visit later in the series. This show was on during TNG, so there's some overlap and funny guest appearances. 

**Premise** Commander Benjamin Sisko loses his ship and wife when the Borg attempt to destroy Earth years earlier during a TNG episode. He, and his son, get assigned a post on the edge of the galaxy at an old spacestation orbiting Bajor -- which is a cross between real-world bosnia and tibet. In the first episode, he discovers a giant wormhole at the edge of the solarsystem. Inside this wormhole is a race of beings who have no concept of time called The Prophets, who end up being real-life Bajoran gods. Bejamin Sisko has on-again off-again contact with The Prophets throughout DS9's seven seasons. 

**Highlights** I always loved the episodes where Captain Sisko is a sci-fi writer in the 1950s who goes crazy. 

**Controversy** A lot of people were very angry that DS9 got so violent during the Dominion War-era shows. It was seen as betraying Gene Roddenberry's stalwart optimism about what humanity was capable of in space.  

#### Battlestar Galactica

This is still my favorite science fiction show of all time, even if Gwen Bell can't sit through it. I've watched it four times all the way through. That's 3200 minutes every time. I started watching Battlestar Galactica in 2006, when I lived in Williamsburg, Brooklyn. I was bored one summer, and wanted to see if there was anything to distract me from that boredom. 

**Premise** Humanity created the Cylons, a race of robots. They fought a big war back in the day, because these robots wanted human rights, and then sent them packing into space. Many years later, the Cylons come back to destroy humanity. In the first thirty minutes of the mini-series, all of the human colonies get nuked. We're left with one surviving Battlestar captained by James Edward Olmos and a fleet of civilian ships Presidented by Donnie Darko's mom. On-going theme: does humanity deserve to survive? Answer: you have to watch the last episode.  

**Highlights** Every single episode from start to finish. The one where Kara Thrace goes into the vortex. Oh man, too many to list. Just watch the thing!

#### Firefly

Firefly is my other favorite scifi show of all time. If only because it's only 14 episodes. It was cancelled very soon after starting because not a lot of people wanted to watch it. Very few people have watched Firefly. It has some of the best cast chemistry, the best insults, and the most fun of any scifi series. It's too bad it got cancelled, but maybe it's all for the best. The good news is they made a movie too, called Serenity. 

**Premise** Western in Space. Ex-Browncoat soldier Captain Reynolds buys a spaceship to 'do business' after losing the war. He fought on the side of the Independents. Unexpected things happen when he takes aboard passangers to have enough gas money, when he finds out he's harboring wanted fugitives Simon and River Tam. After briefly contemplating turning these fugitives into the authorities, "Mal" ends up letting him stay on his ship where they will be safer on the move.  

**Highlights** Janye's Town (hilarious), Out of Gas (wrist slitting depressing)

### Movies

In the order I remembered them.

#### Fifth Element

Flying taxicab driver Bruce Willis gets an unexpected orange-haired girl dumped into his taxicab. Later he finds out she is made up of a type of matter called The Fifth Element, which bad guys want. This event kicks off an interstellar journey to deliver the orange haired girl to the right people, and save the universe from going to hell in a handbasket.

Features giant metal turtles, chubby alien goons, some nudity, and a whacky sense of humor.

#### Tron: Legacy

In the early 80s, Jeff Bridges invents The Grid and then vanishes mysteriously leaving his son all alone to play videogames. Years later, his son releases the source code for the operating system his father originally built, but is now owned by a greedy megacorp. Next up, he gets a mysterious message from his dad. He finds himself in an old arcade, gets zapped by a laser, and vanishes into The Grid where he, Olivia Wilde, and Jeff Bridges attempt to rid The Grid of manipulative algorithms.

Features music by Daft Punk, a beautiful laser light show, and of course Jeff Bridges.

#### Donnie Darko

Jake Gyllenhaal is out sleepwalking when his house is hit by a mysterious falling jet engine. Soon after he meets "Fred" a giant black time-traveling rabbit who encourages Jake to do bad things. He starts a new highschool english class with Drew Berrymore, meets an outcast girl, and exposes a personal development guru for a fraud. But bad things start to happen when Jake begins reading a book on timetravel written by the crazy old lady down the street. 

I won't spoil the end for you, but let's just say: when you try to fork the spacetime continuum, it will find a way to fix itself. For better, or for worse.

#### Serenity

The Firefly Movie. Simon rescues River Tam from the secret government laboratory. Later he finds out they've been experimenting on her, attempting to train her to be a super weapon. Serenity, captained by Malcolm Reynolds, evades a dangerous sword-wielding assassin as they attempt to expose The Truth about what the goverment of the core worlds is doing to its people. All of this, plus Gorramn Reevers.

Features: amazingness

#### Star Trek: First Contact

Captain Picard's arch enemy, The Borg, return to assimilate Earth. All of Starfleet is summoned to destroy them, and they do. But then the borg go back in time and attempt to assimilate Earth before they can defend themselves. The Enterprise follows, destroys the time-traveling Borg ship, only to find the Borg beamed aboard their ship and are assimilating it. No bueno. As the crew of the enterprise fights hand-to-hand with cybernetic zombies in space, fellow crewmates help the creator of Warp Drive with his first flight. Eventually the Vulcans land on Earth and everyone is around for humanity's first contact with intelligent alien life.

#### Blade Runner

In the cyberpunk future, Harrison Ford has one job: retire replicants. Replicants are cylon models created by humans to take out their garbage. When a couple of replicants revolt on Mars, escape to Earth, and attempt to blend in with humanity, Harrison Ford is tasked with finding and retiring each one of them. 

The scenery in Blade Runner is why it's still worth watching, even after all of these years. 

#### Eternal Sunshine of a Spotless Mind

Sad Jim Carrey has a bad breakup with a girl named Clementine. The breakup gets worse when he gets a message a company in Queens telling him that Clementine has had all memories of him erased. Carrey decides to hire this same company to erase his own memory, but something goes wrong and out of order memories start coming back to him. Things get worse when his memory-erasing technician's assistant (Frodo) attempts to impersonate him based on his memories to get Clementine to date him. 

#### Dune

Dune is the most valuable planet in the known universe. Not because of the great scenery, but because of the valuable "Spice" that can be mined from its surface. This spice allows the emporer of the galaxy to move his ships through space in order to maintain Control. Little does anyone know, people already live on the surface of Dune. The Fremen. When Sting's wealthy family loses the contract to mine dune, Maud'dibs family (and bearded Jean Luc Picard) moves from their glorious ocean world to Dune in order to grow richer. Little do they know, this is a plot to kill them. Everyone dies (except for Captain Picard) and Maud'dib and his mom are dumped in the desert to die. 

Once in the desert, Maud'dib realizes that when he's breathing the spice all day he can blow shit up with his mind. He trains the Fremen to blow shit up with their minds, and then they ride giant worms back to the palace to destroy Sting's bloated greedy horrible family.

#### Star Trek: Generations

Captain Kirk is invited to the maiden voyage of the Enterprise B, captained by the depressed kid from Ferriss Bueller's Day Off. Moments after setting sail for the first time, the Enterprise B gets a distress call from a group of El-Aurian transports moving refugees from a far away place where The Borg have destroyed their homeworld. The El-Aurians are trapped in a strange phenomena called The Nexus, a giant swirling rift that travels through space. When the Enterprise B goes to rescue, they get stunk the rift and the only person who can save them is Captain Kirk -- who blows off the front of The Enterprise and throws himself into The Nexus. Everyone considers him dead.

Little do we know, Malcolm McDowell and Whoopie Goldberg were on these transports too. Whoopie is fine being a bartender on The Enterprise, but Malcolm is consumed with trying to "get back" to The Nexus -- a place where nothing bad ever happens and it's Christmas every day.

A few hundred years later, Captain Picard's enterprise is called on to investigate a Klingon attack on a scientific outpost. There they discover Malcolm McDowell's horrible plot to destroy planets in order to cast himself back into the Nexus. Where, coincidentally, Captain Kirk has been hanging out all of this time chopping wood and reliving the life he never had. Captain Picard has a long talk with him about what is reality, and what is not, then they beat the shit out of Malcolm McDowell as The Enterprise crashes into the planet.

#### District 9

In a strange turn of events, aliens come to Earth and humans stick them in a ghetto to subsist on cans of catfood. While the aliens are terrifying in this movie, the real villians are humanity for not letting them get on with their alien lives. Things start to turn around when a human gets bit by one of them and starts to empathize a little bit with their situation.

#### Star Wars (all three)

I can't decide if I'm a Star Wars person. The first three, from the 70s/80s are worthy of note because of their epic landscapes and combat scenes. However, we all know hte story. Luke Skywalker is a Jedi Knight, and Darth is his father. 

Worth watching if you have the flu. 

#### The Fountain

In three different time-periods, Hugh Jackman wants to live forever. In one, he is a conquistador making lives miserable for The Aztecs. In another he is a doctor desperate to save the life of his wife who is ready to die. In a third he travels through space to a far off nebulae in a bubble with a tree as his only friend. As Hugh jumps through these three time periods throughout the movie, you will be left wondering --> will he achieve immortality? 

#### The Matrix

Neo wakes up from his horrible desk job with a message from a group of dissidents over the Internet: What is The Matrix? He gets too curious, only to discover he's been living in a dreamworld. Morpheus unplugs him from The Matrix and convinces him that he is "The One". Bad news is, humanity is enslaved by giant machines using them as batteries. Neo trains up to be The One, only to go visit an oracle who tells him he is not. When Cypher decides to eat Steak and kidnaps Morpheus, Neo is forced to generate as many guns as he can, take these guns into The Matrix, and blow the shit out of some Agent algorithms. Oh, and he can dodge bullets. 

The Matrix will convince you that you too can in fact be The One. The bad news is you have to go back to your normal boring life after the movie ends.

#### Inception

Leonardo Dicaprio has a very funny business, he gives people ideas. Instead of the normal way, he does it by getting together an allstar cast of actors, and Ellen Page to create dreamworlds for people, stick their minds in them, and do battle with their minds until they've come to their own conclusions. This is all, as you might expect, very dangerous work. When Leonardo's ex-lover/suicider starts to show up in his dreamworlds, he knows it's time hire Ellen Page to make worlds for him. 

The best part about this movie is the totems to keep everyone knowing whether they are in the dreamworld or reality. The question is, will they ever made it back?

### Superweird Section

#### Fantastic Planet

Fantastic Planet is superweird. I don't know how to explain it. It might have been the LSD I was on when I watched it the first time. Anyway, weird film involving purple aliens who keep young boys as pets, a race of littles who try to overthrow the big purple aliens. It's all very hard to put a finger on. Also there's a lot of energic telepathic weirdness. Will probably blow your mind and you won't be able to look at reality again the same way. Or maybe that was the LSD. 

### Gwen's Questions for Ev

This piece was inspired by a bunch of questions GB had for me. They are...

+ What sci-fi technology do you most wish you could try?
+ You untether from so much, why haven't you untethered from your sci-fi history?
+ Do you think you're living in a sci-fi world?
+ How has sci-fi impacted your real life?
+ Have you ever written sci-fi? (Because I know that you have.)

Now which one of these movies do you want to watch next? [Email me your answer](mailto:ev@evbogue.com).
