---
title: I want to go to India
date: 2014-05-08
---

I want to go to India at some point in the next few years.

The purpose of this piece is to put my intention out there into the world, and maybe it'll be easier to travel to India the more I talk to people.

Why? Because India and Indians are awesome, as far as I can tell. When I traveled the world in 2012, I enjoyed hanging out with Indians more than anyone in Singapore. I even spent a lot of time at Indian restaurants in Japan! I love Indian food. Give me some Naan and a Curry or Malai Kofta, and I am happy. Oh, and CHAI!

One of my favorite movies is Slumdog Millionaire (I realize this is NOT an Indian movie). I find Bollywood videos amusing and entertaining. 

Besides these things I do not know much about India and Indian culture. However, I want to learn.

I know a significant number of my the readers of this blog are Indian as well, whether living in the USA or in India itself.

I also think living in India would be the ultimate in geoarbitrage. My money would go even farther in India than it does in Mexico.

If you know anything about India, give me a shout so we can chat more about it.

Questions on my mind

1. Where should I go in India?
2. What do I need to know about India?
3. How long can I travel to India for?
4. Where should I live in India?
5. What should I eat in India?

Give me a shout if you have any ideas. You have my contact deets.
