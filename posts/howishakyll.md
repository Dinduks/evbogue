---
title: How is Hakyll
published: 2014-04-13
---

I built evbogue.com on [Hakyll](http://jaspervdj.be/hakyll/), a static website generator written in Haskell.

A number of people have asked about this. Why did I switch off Node.js? Why Hakyll? How is Hakyll?

I hope to answer these questions in this piece. I will attempt to provide installation instructions for an [Arch Linux VPS](http://arch.evbogue.com).

### Why Hakyll?

I was inspired to try Hakyll because of [Gwern](http://gwern.net). Previous to Hakyll, I used a realtime website generator called Bitters that I developed with [Gwen Bell](http://gwenbell.com) as we were learning Node.js.

One of the frustrations I had with Bitters, and Node, is I never could find a way to render a whole bunch of routes using markdown files in a folder. No matter how many 'brilliant' Node programmers I asked about how to do this, no one could answer me with a straight answer or provide a patch that would add this functionality. So adding routes to new documents in Bitters continued to be a pain.

### Goals

1. I want to experience the workflow of a static website generator that works well. Hakyll, after my two days of usage, appears to have
an ideal workflow for a static website.
2. I want to learn more languages. Haskell seems to be fringe enough that few people know it, and mature enough that useful programs have been written in it. I'm also experimenting with using xmonad instead of dwm, because I can program xmonad on the fly with Haskell.

### Installing Haskell

Haskell is not without its headaches.

To get Haskell to work on your Arch Linux system, add the Arch-Haskell repository above Arch-Extra in /etc/pacman.conf. Then you will need add the pacman-key for the lead Haskell developer for Arch. Then you will need to update your keyring. If all of this is confusing to you, [Deploy Arch Linux](http://build.evbogue.com) is a good place to start if you want to get more familiar with the Arch Linux ecosystem. Read the [Haskell Package Guidelines](https://wiki.archlinux.org/index.php/Haskell_package_guidelines) for Arch Linux for more information.

I also tried installing Haskell on Debian and Fedora, and I ran into installation problems. Haskell has some strange dependency issues.
Once I got familiar with them, it became easier to get Haskell to install on my systems.

Here's how I got Haskell to install on Arch Linux, once I added the Arch-Haskell repository.

        $ sudo pacman -Syu ghc cabal-install

This installs the Glorious Haskell Compiler and Cabal Install.

Once you have these two binaries, you need to use Cabal to install the rest of the packages. Do NOT install Haskell Packages from AUR.
This is a terrible experience.

Cabal Install's job is to track libraries. However, some Haskell applications depend on Haskell programs. Cabal won't know if a Haskell
program depends on another Haskell program. If you don't know this going in, you won't know you have to install alex and happy before you can install Hakyll. Do this now, and not after you get error messages.

        $ cabal update

If cabal offers you the option of updating cabal, don't do it. Instead proceed to the next step.

        $ cabal install happy alex

This will install Happy and Alex.

Next, you'll be free to install Hakyll.

        $ cabal install Hakyll

If you're on a desktop your install should finish and Hakyll will be installed.

If you're on a VPS, you'll run into your next problem.

The problem is: GHC takes up so much memory, your installation with probably get a SIGKILL if you're using a VPS with limited RAM. A SIGKILL means you ran out of memory and the virtualization software tells the application to shut down. This will probably happen for you at the point when you're installing pandoc, a requirement of Hakyll.

To overcome this problem, you'll need to add a gigabyte of swap memory to your VPS. Swap memory is virtual memory on your hard disk.

Here's how I did this on Arch Linux.

        $ sudo dd if=/dev/zero of=/swapfile bs=1M count=1024
        $ sudo chmod 600 /swapfile
        $ sudo mkswap /swapfile
        $ sudo swapon /swapfile

You just created a gigabyte of swap memory, which will buffer your install with extra virtual memory so your install doesn't get a SIGKILL.

### The Hakyll workflow

The Hakyll workflow is pretty simple. Run

        $ hakyll-init

to start a new project. If hakyll-init doesn't work, make sure ~/.cabal/bin is in your $PATH by typing

        $ export PATH=$PATH:~/.cabal/bin

This will add the path where Hakyll is located to your $PATH in Linux.

Inside your new project folder, you'll see a number of files.

+ 'site.hs' is the Haskell file where you can program your static site in Haskell
+ 'posts' is where your posts go, in .md format if you want
+ 'templates' is where your templates are. Write your templates in plain old HTML with Haskell-y partials
+ 'css' is where your CSS files go
+ 'images' is where your images go

Edit these files into a site you want to look at, and then you'll want to render them using Haskell into a static website.

First, you'll need to compile your site.hs file into a binary

        $ ghc --make site.hs

Then you'll want to build your site

        $ ./site build

and then you want to preview your site

        $ ./site watch

If these commmands don't work, you'll need to use prefix your command with LANG=en_US.UTF-8, like so

        $ LANG=en_US.UTF-8 ./site build

Or set your global language setting for this change to be permanent

        $ export LANG=en_US.UTF-8

If anything goes wrong, do a clean

        $ ./site clean

When you do a build, it will put your static website in '\_site'. Then you're free to serve this static website however you want, using
any webserver you want. I'm using lighttpd right now.

What do you think about Hakyll? [BitMessage](/penpals) and let me know.


