---
title: Haters page
date: 2014-05-16
---

It's that time of year again. The time when I start getting emails from people who are insane with jealousy at the fact that I manage to pay rent on a [5000 peso apartment](http://evbogue.com/5000pesos/) in la ciudad de Mexico without any support from the US Government. It's time to bring back the hater's page.

People hate me for all sorts of reasons. They hate me because I've been successful in the past. They hate me because I was public when I ran out of money in Berlin in 2012 "Ev Bogue begging for money." And now they hate me because I don't own an iPenis or suck on Zuk's tit. 

It can be useful to break down the hate by the era.

1. Minimalism | 2009 - 2010 

My first haters started coming out when I start getting very into minimalism at the dawn of the 21st century. Why? Because being a hoarder is super stressful, and when you see someone flying around with world with all of their shit stuffed into one bag, it can bring up deep dark feelings of rage. People hated me for being more minimalist than them. They also hated me for pointing out that their lifestyle wasn't working as well as they thought they 'deserved'. Being underwater on a mortgage with a house full of crap can't be fun, so I don't blame the haters.

One anonymous blogger in Brooklyn called me a "douchebag". The label seems to have stuck around. But I think he's a loser, so that's payback.

To this day people still hate me for owning less shit than they do. There is an easy solution to this problem, get a fucking dumpster and throw your shit in it. You will discover, after you part with all of your very important junk, that you no longer want to do me physical harm. 

2. Futurism | 2011

In 2011, I got very into cyborgs and yoga. I started to see exponential curves towards the Singularity. I was almost 100% certain that The Singularity was very near. These days I encourage people to practice [practical transhumanism](http://evbogue.com/transhumanism/). Or perhaps it might be better to call it skeptical transhumanism. Most of the haters from this era were other religious nutsos who believed in different things than me. I see now why the cylons nuked the entire human race, because it's impossible to talk in a rational way with people who believe different things than you do.

3. Traveling the world | 2012

In 2012, I ran out of money after traveling from San Francisco to Singapore to Japan and then to Berlin. To this day, I blame this [running out of money](http://evbogue.com/mistakes/) on the fact that I relied too much on social networks, and I didn't update my website often enough. I also blame Barack Obama, because he was running for dictator of the free world at the time.

Anyway, I admitted defeat in a public way. I said I had no money. I asked for donations "[Ev Bogue Begging for money](http://evbogue.com/moneyadvice/)". I didn't make much money getting donations. Someone did buy my laptop and I was able to get back to los Estados Unidos. What I didn't realize is admitting defeat can really piss the haters off, because most of them have been failing for years. 

It was at this point Charlie Broadway began harassing me. "Is Ev Bogue the most annoying person on earth?" I'm not sure, but I am sure that Charlie Broadway is one of the most psychotic haters I've ever heard from. He's a religious nutso to boot. To this day he continues to get more blog traffic about me than anything he's ever done, because he's never done anything with his life except hate on me.

4. Technical skills | 2013 - 2014

It took awhile to recover from bottoming out on the other side of the world, but I did. Thus began the current era of hate. Who hates me now? No one worth mentioning. Why? Because I have more technical skills than you. I live in a capitalist country, you live in a socialist country. My rent is 5000 pesos, your's is 2500 USD. I have unlimited beer money, and you spend all of your government handouts feeding your car. 

People continue to hate on me because I have technical products, and people buy enough of them for me to afford rent and food. Not a bad life to waste drinking myself to sleep, if you ask me.

The hate will no doubt continue. I will continue to do the work and let the hate mail pile up.
