---
title: iPad Linux: an interview with Miles Moyers
date: 2013-12-26
---

This is an interview with Miles Moyers of [mil3s.com](http://mil3s.com).

I interviewed Miles because of his unique setup. He's using his iPad as a gateway to a Linux VPS. 

Below he explains his setup, the upsides and downsides, and what his ideal setup could be.

**Ev**: How do you use Linux from your iPad?

**Miles**: Hey Ev, what's going on man? How's life...somewhere, wherever you are at :)

Thanks for asking about my Linux set up from my iPad.

It's a little bit complicated so I went ahead and made a quick sketch, showing how it's set up. I hope it's clear, I was at a local watering hole when I drew it up.

<img src="/static/ipadlinux.jpg" class"profile full" />

The way it works is I use my iPad Mini and a terminal emulator. I use the terminal emulator to connect to my two Linux stacks via SSH.

My first stack host my website [mil3s.com](http://mil3s.com), m-i-l-3-s dot com and it's based on a Debian 7 installation on a virtual private server hosted on Digital Ocean.

On that Debian 7 installation I'm running

- Python 2.7
- Dropbox (command line edition)
- Lilac, a Python based static blog generator
- And Nginx as a web server

You can check out Lilac on GitHub it's coded by a young programmer in college. From what my newbee coding mind can see, it's nicely written.

All this to serve [mil3s.com](http://mil3s.com) which is my website hosted outside of the shared environment which I used to have previously with Dreamhost.

My second stack is is an Arch Linux VPS, again hosted with a Digital Ocean.

And on top of that stack I'm running up Python 3.3, which is a newer version. And on top of that I'm running Tmux, a terminal multiplexer which you know very well, and I'm also running Irssi which is my IRC client.

The reason I have two different stacks, is that Arch is a great Linux Distro, but it's a very cutting edge. being so cutting edge, it's libraries are constantly being updated, and it kept breaking my scripts. That is why I added a Debian 7 (stable) installation which is more stable and I use that strictly for serving my website [mil3s.com](http://mil3s.com)

So on my iPad Mini what I do is I use a program called iA Writer, which is a text based editing program where I compose Markdown files that I place into my Dropbox folder, also on my iPad. 

This Dropbox folder syncs with the Dropbox installation on my Debian 7 VPS, and from there, Lilac senses (yes I said senses) changes in the Dropbox Markdown files and regenerates HTML files which are then served by Nginx which go out to the world as [mil3s.com](http://mil3s.com), boy that was a mouthful!

**Ev**: Why did you decide to do it this way? 

**Miles**: Ev, your asking me why I did it this way? Well that is a great question.

I think the big the biggest reason is a ***mobility***.

A few years back in 2007, I had a business that I almost ran completely from an iPhone. After a couple years I shut down the business and took a sabbatical from basically everything except reading. I didn't have a phone, I didn't have a computer, I basically was just reading whatever I can get a hold of. Then when I had enough, I decided to join the living world again, lol.

Living with empty pockets, I didn't really like the fact that I had to lug around laptop to do any work.

I believe that's when I found your Minimalist Business product somewhere on the Internet when you were in the Minimalist Genre and I started to take some of what you wrote to heart.

I started simplifying a lot of the things that I did, including how I worked on the computer.

I told myself technology is changing, I could see technology was changing and I wanted to be able to embrace those changes. Part of being able to embrace those changes is not being stuck to a specific piece of hardware, device, operating system or device.

I think, you're pretty familiar with not getting attached to things.

So part of my strategy, was to force myself to figure out a way to use only an iPad Mini as my device to accomplish whatever I wanted to do online, and functionally instead of using a standard laptop or computer.

At first, as you probably could imagine, things were not as easy as I thought.

I was looking at the Apple products and where I believe they were going in the future, I told myself I would adapt instead of maintaining old habits and old ways of doing things.

Part of adapting for me, was actually using more of the modern ways to input information. As I'm doing right now, this whole interview is being dictated with voice command instead of typed by hand.

I do use the the screen keyboard, to input information but I do not use an external keyboard.

Why? Well, Ev, I'm waiting for the next Technological Renaissance. I believe it will help us, but we need to be ready and able to adapt to it. If not we will go the way of the Dinosaur. 

...I hope my babbling, makes sense, as I am driving down the interstate highway at 80 mph using voice dictation to compose this interview. Only in today's technology...

**Ev**: What are the upsides and downsides?

**Miles**: The upsides are I am extremely mobile. I'm able to utilize virtual private servers to do all of the hard work for me. Instead of having a laptop which I have to lug around, worry about low battery life, and bother with the physical aspect of it. 

With my iPad Mini, I have adapted to reading the screen, i'm able to sit at a coffee shop or pub with very little hardware and accomplish what most people could accomplish with a bulky laptop or desktop. It just takes visualizing the future, and getting ready for it.

The downside? I think the hardware is a little behind the times. Inputting information with your thumbs is not always the easiest thing to do. Voice dictation works great! But entering Python code with voice doesn't really work very well, trust me I keep trying. 

I am waiting to try the one handed keyboard, I think Frogpad makes it, to be iPad compatible, or some cutting edge voice dictation software. Maybe some kind of other cutting edge data input method.

So it's a compromise, but a compromise I'm willing to make, with the expectation that as new technology arrives, I can take full advantage of it.

**Ev**: What is your ideal Linux installation?

**Miles**: Well, Linux on my iPad of course, or an open tablet to run Linux on. 

I like pretty girls, elegant cars and sexy computer equipment. 

- I'm married now, so the pretty girl part is taken care of. 
- I gave up my Land Rover and replaced it with a Mini Cooper to force myself to plan ahead.
- I'm just waiting for a sexy open source compatible tablet, 10", if I had to choose :)

My iPad is probably more capable than the computer equipment that was taken up on Apollo 13.

You're telling me that we can send the man to the moon and bring him back safely, but we can't install Linux on an iPad?

I believe my ideal installation would be my own minimal Linux Distro on a high performance tablet, connecting to my virtual private servers, through high-speed affordably available wide area wifi available to everybody, where I could do my work, and be be able to be creative without barriers.

***

You can visit Miles at [mil3s.com](http://mil3s.com). For those curious, the beer in the above photo is a 'Nutty Brunett'.
