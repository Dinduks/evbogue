---
title: How to clear your mind
date: 2013-12-19
---

So this reader emails me and says, "I don't understand. How come you can reach people, and you're not on Facecrack and Twitter. And I can't reach anyone, and I am on Facecrack and Twitter."

Well duh. Read the sentence above again. The answer is obvious. Still don't get it? I'll break it down for your dumb ass.

You're conflating reach with manipulative social platforms. Just because your brain is getting run around all day in circles by push notifications doesn't mean you're reaching anyone. The job of a for-profit social network, and both of those networks are for profit (for _their_ profit) is to keep you thinking you're important. It's not to give you the tools to disseminate information.

Do yourself a favor, and stop. Take a day off, if you can muster the willpower. Then take two days off. Five days off. A month, a year. Forever. Come back to this piece after you've taken a year off from social networking. 

Good, you're free now. 

Your mind is clear. Now you can sit in front of your computer without constant dopamine jitters. You can open up your terminal, and you can write into it something that means something to other people.

Let's revisit the above sentence one more time. Now that you're off, you can see the obvious. I reach people because I'm not on social networks. I reach people because I write writing people want to read. I'm not sitting in front of my computer all day trying to get a few dozen 'Likes' (or 'MI GUSTAS!' as they call them where I live now. Likes aren't dollars, they're dopamine hits. You used to be a dope fiend, now you're using a computer to reach people.

I'm not saying social networks weren't, at one point in history, good for reaching people. They were, before they turned into hamster wheels for your brain. Now you're hamster, running around in your own self-contained wheel. If you step off the wheel for a good year, you'll see while you were on it you had no way to test if you were 'reaching' people in a definitive way.

And what is reach anyway? Reach is 'The Cloud', it's a marketing term made up by social networks to convince you that you're 'a marketer' while you sit there all day clicking ads and trying to get your daily quota of 'Likes.' And if you pay a few more dollars into the system, maybe you'll get a few more 'Likes.' 

Meanwhile who is making the money? Not you. 

Me.

One more time. Why do I make money while you run yourself around a digital hamster wheel all day?

I don't use social networks. You do. That's why.




