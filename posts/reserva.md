---
title: Reserva, a responsive css framework
date: 2014-01-04
---

Over the past few weeks I hacked together a responsive css framework.

I decided to hack this framework together because I looked at my website on a giant screen, and it looked pretty shitty. Previously, I'd been using Skeleton. But Skeleton appears to be unmaintained, and thus doesn't account for screen sizes larger than those available at the time of its creation.

Reserva is based on Skeleton, but instead of pixels it uses ems and percentages. I re-typed the whole thing by hand, but I did use Skeleton as a reference guide. While Reserva isn't a fork of Skeleton, it is a close cousin by birth.

Want to see Reserve in action? You're in luck, because you're looking at it right now.

I didn't WANT to have to create my own responsive web framework -- however, most of the ones that were cool two years ago have turned into giant bloat monsters. They attempt to account for every possibility, and thus are useless to me. I want the simplest possible CSS framework to get a website deployed to the distributed web. 

So I hacked together Reserva.

Reserva includes a twelve column grid, base styles for your minimalist needs, and many other subtle benefits. 

The point of a css framework is to get out of your way, this gives you the opportunity to have ultimate creativity on top of a solid foundation. Not a shitty bootstrapped foundation.

<a href="http://evbogue.com/static/reserva.css"><button>Download Reserva</button></a>

-Ev 
