---
title: An introduction to Arch Linux 
date: 2014-01-05
---

This is an excerpt from *Build Your Perfect Machine* by Ev Bogue. To find out more, visit [build.evbogue.com](http://build.evbogue.com/) or [email me](mailto:ev@evbogue.com) to learn more.

***

Arch Linux is the most advanced operating system on the planet. Tomorrow, it will be more advanced than it is today.

The brilliance of Arch Linux can be summed up with a single command.

	$ pacman -Syu

This command updates my entire system to the latest version of everything.

Earlier today, I typed this command and received a new version of the Linux kernel, plus the latest version of Vim. 

The Linux kernel is, of course, the famous basis for the most-popular operating system on the planet maintained by [Linus Torvalds](http://www.linfo.org/linus.html). This operating system is called Linux. Vim is a popular open source command-line text editor. 

I'm writing to you using Vim right now. Vim runs on top of the Linux kernel, which (in my case) is distributed under Arch Linux.

***

### What is a Linux distro?

Linux has many distributions.

The most popular distribution is supposed to be Ubuntu. The actual most popular distribution of Linux is Android, because Google forces you to use it on all of their locked-down phones. 

At some point during early 2013, Arch Linux surpassed Ubuntu as the most popular Linux IRC channel. This means a larger number of cool hackery people use Arch Linux than use Ubuntu.

There are other Linux distros. Fedora and Debian have staying power. Gentoo is reported to be more hardcore than Arch Linux, because with Gentoo you are forced to build all of your packages from scratch.

The most hardcore of all Linux distros is LFS. Linux From Scratch. Few have braved these icy waters.

***

### Pacman

When you choose a Linux distro, you're choosing how you want to manage packages. On other distributions you might use apt-get (Ubuntu, Debian) or yum (Fedora). When you use Arch Linux, you'll get access to Pacman.

Pacman updates your entire system with one command

	$ pacman -Syu

Pacman helps you install packages onto your system

	$ pacman -S vim

Pacman helps you remove packages from your system

	$ pacman -R vi

Arch Linux has a rolling release schedule. This means you get updates to Arch Linux as soon as they are available. This is different from other operating systems where you have to wait until a new version of the entire operating system is released.

Because you get access to all of the latest packages as soon as they are uploaded to the package manager, you're responsible for yourself. If something goes wrong with your Arch Linux installation, it's your responsibility to fix the problem. This means no help lines, no call centers, no scapegoats.

If you can't handle the pressure, install a Linux distro with a support staff.

If you want to be on the cutting edge, install Arch Linux.

***

### The Arch Way

Arch Linux is shaped by a strict philosophy of simplicity, code-correctness over convenience, user-centric, openness, and freedom. This is called [The Arch Way](https://wiki.archlinux.org/index.php/The_Arch_Way).

The biggest advantage of this philosophy is it encourages users to get curious about their own machines. Where other Linux distros choose all of the applications you will need, and pre-install them on your system, Arch Linux forces you to choose the applications you want. 

When you first install Arch Linux, you'll have a bare-bones system without even a graphical frontend. 

Your computer will boot to a black screen with a bash prompt.

Then you can use Pacman to install the programs you want on your system. This puts the choice of how you want to build your system on your shoulders, instead of a team of developers somewhere who think they know what is right for you.

***

### Build your ideal system

When you install Arch, all you get is a bash prompt, pacman, and essential system programs you need to boot a machine.

What you put on your machine is up to you. This means you get to make choices around the graphical user interface (GUI) you install. It means you get to get make choices about what applications and development tools you install.

This choice can be rewarding, but it can also be overwhelming. You might want to install Gnome

	$ pacman -S gnome

But maybe you want to install LXDE

	$ pacman -S lxde

Or perhaps you want to install dwm 

	$ pacman -S dwm

Perhaps you want to install all of these GUIs and switch between them until you figure out which one you enjoy having more.

When you're using Arch Linux you get to make choices about what you install on your machine, and how you use it. This gives you freedom to grow and change as a human who uses a computer. Instead of just using what everyone else is using, you can experiment with exciting different and/or new technologies.

***
