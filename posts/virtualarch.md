---
title: Virtual Arch Linux
date: 2014-01-06
---

This is an excerpt from *Build Your Perfect Machine* by Ev Bogue. To find out more, visit [build.evbogue.com](http://build.evbogue.com/) or [email me](mailto:ev@evbogue.com) to learn more.

***

The best way to get started deploying Arch Linux is using it via a VPS (virtual private server). Then you can avoid the potential heartache of attempting to install it on your own system. 

Using Arch on a VPS allows you to experiment with a new system from the safety and comfort of the system you're used to using. Even if it's not quite as awesome as Arch Linux, your old system will do for now. 

In time you will see the wisdom of Arch Linux, and you won't be able to look away from it. One night you will wake up in a cold sweat, erase your entire hard drive and install Arch Linux from a USB stick in less than twenty minutes (with the assistance of Deploy Arch Linux.)

In the meantime, let's get an Arch Linux VPS up and running. Then you can experiment with the Arch Linux command line without all of the commitment to erasing your hard drive and other hard-to-imagine possibilities.

### Obtain a VPS

Start by getting a [$5 VPS from Digital Ocean](https://www.digitalocean.com/?refcode=26d8ed49730d) (rewards link). 

The double benefit of this tutorial is by the end you will know how to use a VPS to deploy applications to the web. 

Here's how to get a VPS fired up at Digital Ocean

1. Create an account at [Digital Ocean](https://www.digitalocean.com/?refcode=26d8ed49730d)
2. Click the big green 'Create' button
3. Type in a hostname. 'arch' can do, if you can't think of anything
4. Select your size. 512MB will do at $5 a month
5. Select your region. Do you want to be in New York, San Francisco, or Amsterdam? You want to be at NY2, your VPS will be in the most valuable building in New york at the tip of the fiber tunnel.
6. Select your distribution. Arch Linux, 64-bit
7. Leave the Settings field at its defaults
8. Hit the big green Create Droplet button

In the next few moments, Digital Ocean will email you a username, password, and its IP address.

You'll receive an email similar to this:

	Yay! Your new Droplet has been created!

	You can access it using the following credentials:

	IP Address: 162.243.100.29
	Username: root
	Password: vcncizdhblli

	INSTRUCTIONS

	To login to your droplet, you will need to open a terminal window and copy and paste the following string:

	ssh root@162.243.100.29

	Once you've entered the string, copy and paste your password:

	vcncizdhblli

	If you're having trouble, please visit the following tutorial:
	https://www.digitalocean.com/community/articles/how-to-create-your-first-digitalocean-droplet-virtual-server

	Happy coding,
	DigitalOcean

To login to your droplet, open up your computer's terminal and type

	$ ssh root@192.242.100.29
	The authenticity of host '162.243.100.29 (162.243.100.29)' can't be established.
	ECDSA key fingerprint is f7:54:94:f2:2e:d1:cf:0c:fb:c5:05:9e:74:20:d9:a9.
	Are you sure you want to continue connecting (yes/no)? yes
	Warning: Permanently added '162.243.100.29' (ECDSA) to the list of known hosts.
	root@162.243.100.29's password:	

Type or paste your password in, and you'll enter the Arch Linux VPS.

### Update Arch Linux

The most magical tool in Arch Linux is Pacman, the Arch Linux package manager. 

Pacman lets you update your system with a single command

	$ pacman -Syu

On your Digital Ocean VPS, you'll need to type it with one additional argument. Because the Digital Ocean droplet you're using is slightly old, and old Arch Linux installations are known to complain a bit.

Type this into your Arch Linux VPS to update it to the latest version

	$ pacman -Syu --ignore filesystem

Once you type the command, you'll see a large amount of output 
	
	:: Synchronizing package databases...
	 core is up to date
	 extra is up to date
	 community is up to date
	:: Starting full system upgrade...
	warning: filesystem: ignoring package upgrade (2013.03-2 => 2013.05-2)
	:: Replace heirloom-mailx with core/s-nail? [Y/n] y
	warning: linux: ignoring package upgrade (3.8.4-1 => 3.12-1)
	resolving dependencies...
	looking for inter-conflicts...

	Packages (119): acl-2.2.52-2  archlinux-keyring-20131027-1  attr-2.4.47-1
                automake-1.14-1  bash-4.2.045-5  binutils-2.23.2-3
                bison-3.0.1-1  bzip2-1.0.6-5  ca-certificates-20130906-1
                cloog-0.18.1-2  coreutils-8.21-2  cracklib-2.9.0-2
                cronie-1.4.9-5  cryptsetup-1.6.2-2  curl-7.33.0-3
                db-5.3.28-1  dbus-1.6.16-1  device-mapper-2.02.104-1
                dhcpcd-6.1.0-1  e2fsprogs-1.42.8-2  expat-2.1.0-3
                fakeroot-1.20-1  file-5.15-1  gawk-4.1.0-2  gc-7.2.d-2
                gcc-4.8.2-4  gcc-libs-4.8.2-4  gdbm-1.10-3
                gettext-0.18.3.1-2  glib2-2.38.2-1  glibc-2.18-10
                gmp-5.1.3-2  gnupg-2.0.22-1  gpgme-1.4.3-1  grep-2.15-1
                groff-1.22.2-5  guile-2.0.9-1  gzip-1.6-1
                heirloom-mailx-12.5-3 [removal]  hwids-20130607-1
                ifplugd-0.28-14  inetutils-1.9.1.341-1  iproute2-3.11.0-1
                iptables-1.4.20-1  iputils-20121221-3  isl-0.12.1-2
                jfsutils-1.1.15-4  kbd-2.0.1-1  keyutils-1.5.8-1  kmod-15-1
                krb5-1.11.4-1  libarchive-3.1.2-4  libassuan-2.1.1-1
                libedit-20130601_3.1-1  libffi-3.0.13-4  libgcrypt-1.5.3-1
                libgpg-error-1.12-1  libgssglue-0.4-2  libldap-2.4.37-1
                libltdl-2.4.2-12  libmpc-1.0.1-2  libnl-3.2.22-1
                libpipeline-1.2.4-1  libsasl-2.1.26-6  libssh2-1.4.3-2
                libtirpc-0.2.3-2  libtool-2.4.2-12  libunistring-0.9.3-6
                libusbx-1.0.17-1  linux-api-headers-3.10.6-1
                linux-firmware-20131013.7d0c7a8-1  logrotate-3.8.7-1
                lvm2-2.02.104-1  lzo2-2.06-3  m4-1.4.17-1  make-4.0-1
                man-db-2.6.5-1  man-pages-3.54-1  mdadm-3.3-2
                mkinitcpio-0.15.0-1  mkinitcpio-busybox-1.21.1-2
                mpfr-3.1.2.p4-1  ncurses-5.9-6
                net-tools-1.60.20130531git-1  nmap-6.40-1  openssh-6.4p1-1
                openssl-1.0.1.e-5  pacman-4.1.2-4
                pacman-mirrorlist-20130830-1  pam-1.1.8-2
                pambase-20130928-1  pciutils-3.2.0-4  pcre-8.33-2
                perl-5.18.1-1  pkgfile-11-1  popt-1.16-7  ppl-1.1-1
                ppp-2.4.5-8  procps-ng-3.3.8-3  readline-6.2.004-2
                reiserfsprogs-3.6.24-1  rsync-3.1.0-1  run-parts-4.4-1
                s-nail-14.4.5-1  sed-4.2.2-3  shadow-4.1.5.1-7
                sudo-1.8.8-1  syslinux-6.02-4  systemd-208-2
                systemd-sysvcompat-208-2  sysvinit-tools-2.88-12
                tar-1.27.1-1  texinfo-5.2-2  tzdata-2013h-1  usbutils-007-1
                util-linux-2.24-1  xfsprogs-3.1.11-2  xz-5.0.5-2
                zlib-1.2.8-3

	Total Download Size:    122.74 MiB
	Total Installed Size:   471.78 MiB
	Net Upgrade Size:       -48.50 MiB

	:: Proceed with installation? [Y/n]

Type 'y' and it will update all of Arch Linux to the latest version. Because the Linux image Digital Ocean uses is so old, almost every package will need to be updated to the latest version. This is good! It means you're running the most current Linux distribution on the planet. 

Now type

        $ pacman -Syu

To install the 'filesystem' package you ignored earlier. You had to do this because the filesystem package depends on another change that comes before it.

Now you're running the latest version of Arch Linux on your VPS.

***

### Settle into Arch Linux

Now you have an updated and fully operational Arch Linux VPS. What will you do with it now?

A good first step is to change your root password. This way no one will be able to gain unauthorized access to your VPS via the password Digital Ocean sent you. To change the password of your VPS type

	$ passwd 
	Enter new UNIX password:
	Retype new UNIX password:
	passwd: password updated successfully

Now log out and log in to make sure you remembered your password. If you forgot it, destroy the VPS and start over from the beginning.

To log out type
	
	$ exit

Log back in using the ip address Digital Ocean sent you.

	$ ssh root@198.168.1.1

Next you'll need to create a new user on your system. This is the user you'll use to log into your system for regular usage.

	$ useradd -m -g wheel -s /bin/bash evbogue

This will create a user...

1. With a home folder (-m)
2. In the group wheel (-g)
3. With the bourne again shell (-s /bin/bash)
4. With the username of evbogue (you'll of course want to sub this out for your own username)

To set your new users' password

	$ passwd evbogue
	Enter new UNIX password:
	Retype new UNIX password:
	passwd: password updated successfully

You already did this above. Before we test to see if this password took, we'll give this user sudo privileges.

Sudo is the name we use for the application giving normal users superuser privileges. Hence, sudo. Super User Do.

To give your user sudo privileges, we're going to use a tool called visudo to gift every user in the %wheel group sudo privileges. 

First, you'll need a decent text editor. We're going to use Vim. To install vim on Arch Linux, type.

	$ pacman -S vim

Next, you'll need to tell your system which editor you want to use with visudo 

	$ export EDITOR=vim
	
And now launch visudo

	$ visudo

This will launch the sudoers file. Scroll down to you where you see this section

	##
	## User privilege specification
	##
	root ALL=(ALL) ALL

	## Uncomment to allow members of group wheel to execute any command
	# %wheel ALL=(ALL) ALL

	## Same thing without a password
	# %wheel ALL=(ALL) NOPASSWD: ALL

And uncomment the %wheel user by deleting the # from in front of it. This will give any user in the wheel group access to sudo.

It'll look this way, once you uncomment %wheel. You should ONLY remove the '#' from in front of %wheel.

	##
	## User privilege specification
	##
	root ALL=(ALL) ALL

	## Uncomment to allow members of group wheel to execute any command
	%wheel ALL=(ALL) ALL
	
	## Same thing without a password
	# %wheel ALL=(ALL) NOPASSWD: ALL

Now type 

	:wq

To save with Vim.

Now your new user will have sudo privileges.

Log out 

	$ exit

and log in using your new user

	$ ssh evbogue@192.168.1.154

If everything went well, you'll log into the user you created.


